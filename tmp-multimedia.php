<?php
/*
 * Template name: multimedia
 */

get_header();?>

        <div class="inner-page-block">
            <div class="inner-page-banner-img">
                <img src="<?php echo TEMPLATE_DIR_URI; ?>/images/header-banner-media.png" alt="" />
            </div>


            <div class="inner-content">
                <div class="decorative-borders-center"></div>

                <h2 class="heading text-center hidden-xs">
                    <img src="<?php echo TEMPLATE_DIR_URI; ?>/images/heading-lg-media.png" alt=""/>
                </h2>

                <h2 class="heading text-center visible-xs">
                    <img src="<?php echo TEMPLATE_DIR_URI; ?>/images/heading-xs-media.png" alt=""/>
                </h2>

                <div class="news-content">
<?php

$term_id = 0;
$base_url = home_url('/media/');

if( ! empty($_GET['t']) && is_numeric($_GET['t']) ) {
  $term_id = $_GET['t'];
}

$args = array(
    'hide_empty' => false,
    'orderby' => 'term_order',
    'exclude' => 1,
);

$media_cats = get_terms( 'media_cat', $args );
?>
                    <ul class="news-category-tab">
                        <li <?php if( $term_id == 0 ): ?>class="active"<?php endif; ?>><a href="<?php echo $base_url; ?>">最新</a></li>
<?php
if( ! is_wp_error($media_cats) ):
  foreach( $media_cats as $media_cat ):
?>
                        <li <?php if( $term_id == $media_cat->term_id ): ?>class="active"<?php endif; ?>><a href="<?php echo add_query_arg( 't', $media_cat->term_id, $base_url ); ?>"><?php echo $media_cat->name; ?></a></li>
<?php
  endforeach;
endif;
?>
                    </ul>
<?php

$media_list_paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;

$args = array(
    'post_type'      => 'multimedia',
    'post_status' => 'publish',
    'paged'	         => $media_list_paged,

);

if( $term_id > 0 ) {
  $args['tax_query'] = array(
                          array(
                              'taxonomy' => 'media_cat',
                              'terms' => $term_id,
                          )
                       );

}


$post_query = new WP_Query( $args );
if ( $post_query->have_posts() ) :
?>

                    <div class="media-list">
                        <ul>
<?php
  while ( $post_query->have_posts() ) :
    $post_query->the_post();
    $ID = get_the_ID();

    $is_video = true;
    $mfp_class = 'mfp-iframe';
    $video_url = get_post_meta( $ID, POST_META_VIDEO, true );

    if( empty( $video_url ) ) {
      $video_url = get_the_post_thumbnail_url( $ID, 'full');
      $mfp_class = 'mfp-image';
      $is_video = false;
    };

    $thumb_nail = get_the_post_thumbnail_url( $ID, 'video-cover');
?>
                            <li class="media-list-item">
                                <a class="video-cover <?php echo $mfp_class; ?>" href="<?php echo $video_url ?>" style="background-image:url('<?php echo $thumb_nail; ?>')">
                                <?php if($is_video): ?>
                                    <i class="fa fa-play-circle" aria-hidden="true"></i>
                                <?php endif; ?>
                                </a>
                                <div class="video-title"><?php the_title(); ?></div>
                            </li>
<?php
  endwhile;
?>


                        </ul>
                    </div>

<?php
else:
?>
                    <div class="news-list" style="color:#fff;">找不到影音圖集</div>
<?php
endif;

if ($post_query->max_num_pages > 1):
  numeric_posts_nav($post_query);
endif;

?>
                </div>
            </div>
        </div>
<script>
jQuery(document).ready(function($){

  // magnific popup
  $('a.video-cover').magnificPopup({
    gallery: {
      enabled: true
    }
  });

});

</script>

<?php

wp_reset_postdata();

get_footer();
