<?php

if( defined( 'WP_CLI' ) && WP_CLI ) {

  class DN_Export {

    function export_dn_user ( $args, $assoc_args ) {


      $content = "註冊序號\t註冊時間\t註冊Email\t手機號碼\t公會序號\t投票記錄\t來源\tCLIENT ID\tTOKEN FOR BUSINESS\n";

      global $wpdb;
      $tbl_name = $wpdb->prefix . DN_USER_TABLE;
      $sql = "SELECT * FROM $tbl_name order by ID";

      $result = $wpdb->get_results( $sql, 'ARRAY_A' );

      foreach($result as $dn_user) {
        $content .= $dn_user['ID'] . "\t";
        $content .= get_date_from_gmt( $dn_user['user_registered'] ) . "\t";
        $content .= $dn_user['user_email'] . "\t";
        $content .= $dn_user['phone_number'] . "\t";
        $content .= $dn_user['guild'] . "\t";
        $content .= $dn_user['voted_video'] . "\t";
        $content .= $dn_user['login_type'] . "\t";
        $content .= $dn_user['client_id'] . "\t";
        $content .= $dn_user['token_for_business'] . "\t";
        $content .= "\n";
      }


      $fp = fopen( "dn_user_" . time() . ".csv", 'w' );
      fwrite($fp, $content);
      fclose($fp);

      WP_CLI::success( "export_dn_user done." );
    }

    function export_guild (  $args, $assoc_args ) {
      $content = "公會序號\t創建時間\t公會名稱\t成員數量\t成員列表\n";

      $args = array(
          'post_type'      => 'guild', // taxonomy name
          'orderby'       => 'ID',
          'order'         => 'ASC',
          'post_status'    => 'publish',
          'nopaging' => true,
      );

      $posts = get_posts( $args );

      foreach( $posts as $guild ){
        $args = array(
          'meta_key' => USER_META_GUILD_ID,
          'meta_value' => $guild->ID,
          'orderby' => 'display_name',
          'fields' => array('display_name'),
          'count_total' => true,
        );
        $users = new WP_User_Query( $args );
        $member_list = [];

        if ( ! empty( $users->results ) ) {
          foreach( $users->results as $u ) {
            $member_list[] = '"' . $u->display_name . '"';
          }
        }

        $content .= $guild->ID . "\t";
        $content .= $guild->post_date . "\t";
        $content .= $guild->post_title . "\t";
        $content .= $users->get_total() . "\t";
        $content .= implode(',', $member_list) . "\t";

        $content .= "\n";
      }

      $fp = fopen( "dn_guild_" . time() . ".csv", 'w' );
      fwrite($fp, $content);
      fclose($fp);

      WP_CLI::success( "export_guild done." );
    }
  }

  WP_CLI::add_command( 'dn-export', 'DN_Export' );
}