<?php

get_header();?>

        <div class="row">
            <div class="col-sm-12">
                <div class="landing-hero  hidden-xs">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="media-list-item">
<?php
$home_video = get_option(SITE_VIDEO_HOME);
$home_video_thumb = get_option(SITE_VIDEO_HOME_THUMB);
// $home_video_thumb = getYoutubeThumb($home_video);

if ( ! empty( $home_video ) ):
?>
                                         <a class="home-video video-cover mfp-iframe" href="<?php echo $home_video; ?>" style="background:url('<?php echo $home_video_thumb; ?>') center center no-repeat;">
                                            <i class="fa fa-play-circle" aria-hidden="true"></i>
                                        </a>
                                        <div class="video-title">龍之谷遊戲影片介紹</div>
<?php endif; ?>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="download-area">
                                        <a href="<?php echo get_option(SITE_DOWNLOAD_IOS); ?>"><img src="<?php echo TEMPLATE_DIR_URI; ?>/images/ios-download.png" /></a>
                                        <a href="<?php echo get_option(SITE_DOWNLOAD_ANDROID); ?>"><img src="<?php echo TEMPLATE_DIR_URI; ?>/images/google-play.png" /></a>
                                        <a href="<?php echo get_option(SITE_DOWNLOAD_OFFICIAL); ?>"><img src="<?php echo TEMPLATE_DIR_URI; ?>/images/android-download.png" /></a>
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="qr-code-area">
                                        <img src="<?php echo QRCODE_IMG; ?>" />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="landing-key-img">
                                <img src="<?php echo TEMPLATE_DIR_URI; ?>/images/header-banner-landing.png" />
<?php
$pre_reg_dueday = strtotime(get_option(SITE_PRE_REG_DUEDAY));
if( ! ( $pre_reg_dueday !== false && time() > $pre_reg_dueday ) ):
?>
                                <a class="preregister-bg" href="<?php echo home_url('/pre-reg/'); ?>">
                                    <span class="preregister-title"></span>
                                </a>
<?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--Hero 手機版-->
        <div class="row visible-xs-block">
            <div class="col-xs-12">
                <div class="landing-hero-mobile">
                    <img src="<?php echo TEMPLATE_DIR_URI; ?>/images/header-banner-landing.png" />

                    <div class="media-list-item">
                        <a class="home-video video-cover mfp-iframe" href="<?php echo $home_video; ?>" style="background:url('<?php echo $home_video_thumb; ?>') center center no-repeat;">
                            <i class="fa fa-play-circle" aria-hidden="true"></i>
                        </a>
                        <div class="video-title">龍之谷遊戲影片介紹</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row visible-xs-block">
            <div class="col-xs-12">
                <div class="landing-service-btn">
                    <a class="download-game-bg" href="<?php echo get_option(SITE_DOWNLOAD_OFFICIAL); ?>">
                        <span class="download-game-title"></span>
                    </a>

                    <a class="btn btn-default btn-lg btn-block" href="/code/">
                        <i class="fa fa-gift" aria-hidden="true"></i> 兌換虛寶
                    </a>
                    <a class="btn btn-default btn-lg btn-block" href="https://funplus.helpshift.com/a/龍之谷/">
                        <i class="fa fa-phone" aria-hidden="true"></i> 客服支援
                    </a>
                    <a class="btn btn-default btn-lg btn-block" href="http://dn-shop.funplusgame.com">
                        <i class="fa fa-plus" aria-hidden="true"></i> 儲值方式
                    </a>
                </div>

            </div>
        </div>

        <!--最新消息區-->
        <div class="row  hidden-xs">
            <div class="col-sm-12">
                <div class="landing-news-block">
                    <div class="row">
                        <div class="col-lg-5 col-md-4 col-sm-5">
                            <h3><img src="<?php echo TEMPLATE_DIR_URI; ?>/images/heading-xs-news.png" /></h3>
                            <div class="game-info">
                                <a href="<?php echo GAMER_GROUP_URL; ?>">
                                    <img src="<?php echo TEMPLATE_DIR_URI; ?>/images/banner-gamer.png" />
                                </a>
                                <a href="<?php echo FB_PAGE_URL; ?>">
                                    <img src="<?php echo TEMPLATE_DIR_URI; ?>/images/banner-facebook.png" />
                                </a>

                            </div>

                        </div>
<?php
$args = array(
    'post_type'      => 'post',
    'post_status' => 'publish',
    'posts_per_page' => 6,
);


$post_query = new WP_Query( $args );

?>
                        <div class="col-lg-7 col-md-8 col-sm-7">
                            <div class="landing-news">
                                <div class="news-heading">
                                    <h4>消息列表</h4>
                                    <a class="more-news-btn" href="<?php echo home_url('/news/'); ?>">
                                        更多消息 <i class="fa fa-chevron-right" aria-hidden="true"></i>
                                    </a>
                                </div>

                                <div class="news-list-block">
                                    <ul>
<?php
while ( $post_query->have_posts() ) :
  $post_query->the_post();
?>
                                        <li class="news-list-item">
                                            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                                                <div class="news-list-icon">
                                                    <?php the_post_thumbnail('home-news-avatar'); ?>
                                                </div>
                                                <div class="news-list-title">
                                                    <strong><?php the_title(); ?></strong>
                                                    <span><?php the_time('Y/m/d'); ?></span>
                                                </div>
                                            </a>
                                        </li>
<?php
endwhile;
?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--最新消息 手機版-->
    <div class="landing-news-mobile  visible-xs-block">
        <h3 class="text-center space-bottom-30 mobile-heading"><img src="<?php echo TEMPLATE_DIR_URI; ?>/images/heading-xs-news.png" /></h3>

        <div class="news-list">
            <ul>
<?php
while ( $post_query->have_posts() ) :
  $post_query->the_post();
?>

                <li class="news-list-item">
                    <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                        <div class="news-list-icon">
                            <?php the_post_thumbnail('home-news-avatar'); ?>
                        </div>
                        <div class="news-list-title">
                            <span><?php the_time('Y/m/d'); ?></span>
                            <strong><?php the_title(); ?></strong>
                        </div>
                    </a>
                </li>
<?php
endwhile;


?>
            </ul>
        </div>



        <div class="space-top-20 text-center  space-bottom-10 visible-xs-block">
            <a href="<?php echo home_url('/news/'); ?>" class="btn btn-primary submit-btn">更多消息 <i class="fa fa-chevron-right" aria-hidden="true"></i></a>

        </div>
    </div>

    <!--職業介紹-->
    <div class="landing-classes-block hidden-xs">
        <div class="container">
            <div class="row">
                <div class="col-sm-3">
                    <!--戰士-->
                    <div class="landing-classes-info classes-1 active ">
                        <h3><img src="<?php echo TEMPLATE_DIR_URI; ?>/images/heading-xs-classes.png" /></h3>
                        <div class="landing-classes-title">
                            <strong>戰士</strong>
                            <span>Warrior</span>
                        </div>
                        <p class="landing-classes-description">
                            戰士是勇猛和力量的化身，作為最驍勇善戰的他們，擁有最快奔跑速度，掌握近戰組合打擊能力和一定的遠程劍氣、旋風攻擊能力，並能在戰場給予隊友護體支援。
                        </p>
                        <div class="space-top-30">
                            <img class="classes-avatar" src="<?php echo TEMPLATE_DIR_URI; ?>/images/warrior-avatar.png" />
                        </div>
                    </div>
                    <!--弓箭手-->
                    <div class="landing-classes-info classes-2">
                        <h3><img src="<?php echo TEMPLATE_DIR_URI; ?>/images/heading-xs-classes.png" /></h3>
                        <div class="landing-classes-title">
                            <strong>弓箭手</strong>
                            <span>Archer</span>
                        </div>
                        <p class="landing-classes-description">
                            弓箭手是靈動和迅捷的化身，作為出生於生命之樹的精靈族的他們靜如松木動如脫兔，擁有最遠距離的箭術打擊能力和不遜於任何人的體術格鬥能力，並能在戰場中快速穿梭。
                        </p>
                        <div class="space-top-30">
                            <img class="classes-avatar" src="<?php echo TEMPLATE_DIR_URI; ?>/images/archer-avatar.png" />
                        </div>
                    </div>
                    <!--魔法師-->
                    <div class="landing-classes-info classes-3">
                        <h3><img src="<?php echo TEMPLATE_DIR_URI; ?>/images/heading-xs-classes.png" /></h3>
                        <div class="landing-classes-title">
                            <strong>魔法師</strong>
                            <span>Magician</span>
                        </div>
                        <p class="landing-classes-description">
                            法師是法術和智慧的化身。作為人類聯盟的一員，擁有最強大的魔法打擊能力和不輸於任何人的戰場控制能力，並能在戰場中快速穿梭。
                        </p>
                        <div class="space-top-30">
                            <img class="classes-avatar" src="<?php echo TEMPLATE_DIR_URI; ?>/images/magician-avatar.png" />
                        </div>
                    </div>
                    <!--牧師-->
                    <div class="landing-classes-info classes-4">
                        <h3><img src="<?php echo TEMPLATE_DIR_URI; ?>/images/heading-xs-classes.png" /></h3>
                        <div class="landing-classes-title">
                            <strong>牧師</strong>
                            <span>Minister</span>
                        </div>
                        <p class="landing-classes-description">
                            牧師是光明和正義的化身，作為神殿騎士的一員，他們擁有無與倫比氣質和戰鬥素養，在團隊內適合擔任領袖，在戰鬥中保護隊友和聚合團隊的力量是他們的首要任務。
                        </p>
                        <div class="space-top-30">
                            <img class="classes-avatar" src="<?php echo TEMPLATE_DIR_URI; ?>/images/minister-avatar.png" />
                        </div>
                    </div>

                </div>
                <div class="col-sm-9">
                    <div class="landing-classes-content ">
                        <img class="landing-classes-portrait active" src="<?php echo TEMPLATE_DIR_URI; ?>/images/warrior-portrait.png" />
                        <img class="landing-classes-portrait" src="<?php echo TEMPLATE_DIR_URI; ?>/images/archer-portrait.png" />
                        <img class="landing-classes-portrait" src="<?php echo TEMPLATE_DIR_URI; ?>/images/magician-portrait.png" />
                        <img class="landing-classes-portrait" src="<?php echo TEMPLATE_DIR_URI; ?>/images/minister-portrait.png" />


                        <ul class="landing-classes-list">
                            <li class="classes-list-item classes-1 active">戰士</li>
                            <li class="classes-list-item classes-2">弓箭手</li>
                            <li class="classes-list-item classes-3">魔法師</li>
                            <li class="classes-list-item classes-4">牧師</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--職業介紹 手機版-->

    <div class="landing-classes-mobile  visible-xs-block">
        <h3 class="text-center mobile-heading"><img src="<?php echo TEMPLATE_DIR_URI; ?>/images/heading-xs-classes.png" /></h3>

        <div class="landing-classes-slider">
            <div class="classes-content-mobile">
                <img class="landing-classes-portrait-mobile" src="<?php echo TEMPLATE_DIR_URI; ?>/images/warrior-portrait.png" />
                <div class="classes-info-mobile">
                    <div class="landing-classes-title">
                        <strong>戰士</strong>
                        <span>Warrior</span>
                    </div>
                    <p class="landing-classes-description">
                        戰士是勇猛和力量的化身，作為最驍勇善戰的他們，擁有最快奔跑速度，掌握近戰組合打擊能力和一定的遠程劍氣、旋風攻擊能力，並能在戰場給予隊友護體支援。
                    </p>
                    <div class="space-top-30">
                        <img class="classes-avatar" src="<?php echo TEMPLATE_DIR_URI; ?>/images/warrior-avatar.png" />
                    </div>
                </div>
            </div>

            <div class="classes-content-mobile">
                <img class="landing-classes-portrait-mobile" src="<?php echo TEMPLATE_DIR_URI; ?>/images/archer-portrait.png" />
                <div class="classes-info-mobile">
                    <div class="landing-classes-title">
                        <strong>弓箭手</strong>
                        <span>Archer</span>
                    </div>
                    <p class="landing-classes-description">
                        弓箭手是靈動和迅捷的化身，作為出生於生命之樹的精靈族的他們靜如松木動如脫兔，擁有最遠距離的箭術打擊能力和不遜於任何人的體術格鬥能力，並能在戰場中快速穿梭。
                    </p>
                    <div class="space-top-30">
                        <img class="classes-avatar" src="<?php echo TEMPLATE_DIR_URI; ?>/images/archer-avatar.png" />
                    </div>
                </div>
            </div>

            <div class="classes-content-mobile">
                <img class="landing-classes-portrait-mobile" src="<?php echo TEMPLATE_DIR_URI; ?>/images/magician-portrait.png" />
                <div class="classes-info-mobile">
                    <div class="landing-classes-title">
                        <strong>魔法師</strong>
                        <span>Magician</span>
                    </div>
                    <p class="landing-classes-description">
                        法師是法術和智慧的化身。作為人類聯盟的一員，擁有最強大的魔法打擊能力和不輸於任何人的戰場控制能力，並能在戰場中快速穿梭。
                    </p>
                    <div class="space-top-30">
                        <img class="classes-avatar" src="<?php echo TEMPLATE_DIR_URI; ?>/images/magician-avatar.png" />
                    </div>
                </div>
            </div>

            <div class="classes-content-mobile">
                <img class="landing-classes-portrait-mobile" src="<?php echo TEMPLATE_DIR_URI; ?>/images/minister-portrait.png" />
                <div class="classes-info-mobile">
                    <div class="landing-classes-title">
                        <strong>牧師</strong>
                        <span>Minister</span>
                    </div>
                    <p class="landing-classes-description">
                        牧師是光明和正義的化身，作為神殿騎士的一員，他們擁有無與倫比氣質和戰鬥素養，在團隊內適合擔任領袖，在戰鬥中保護隊友和聚合團隊的力量是他們的首要任務。
                    </p>
                    <div class="space-top-30">
                        <img class="classes-avatar" src="<?php echo TEMPLATE_DIR_URI; ?>/images/minister-avatar.png" />
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--遊戲特色-->
    <div class="container hidden-xs">
        <div class="row">
            <div class="col-sm-12">
                <h3 class="text-center space-top-50"><img src="<?php echo TEMPLATE_DIR_URI; ?>/images/heading-game-feater.png" /></h3>

                <div class="landing-game-feature">
                    <div>
                        <img src="<?php echo TEMPLATE_DIR_URI; ?>/images/game-feature-1.jpg" />
                    </div>
                    <div>
                        <img src="<?php echo TEMPLATE_DIR_URI; ?>/images/game-feature-2.jpg" />
                    </div>
                    <div>
                        <img src="<?php echo TEMPLATE_DIR_URI; ?>/images/game-feature-3.jpg" />
                    </div>
                    <div>
                        <img src="<?php echo TEMPLATE_DIR_URI; ?>/images/game-feature-4.jpg" />
                    </div>
                    <div>
                        <img src="<?php echo TEMPLATE_DIR_URI; ?>/images/game-feature-5.jpg" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--遊戲特色 手機版-->
    <div class="game-feature-mobile space-top-20  visible-xs-block">
        <h3 class="text-center mobile-heading"><img src="<?php echo TEMPLATE_DIR_URI; ?>/images/heading-game-feater.png" /></h3>

        <div class="game-feature-mobile-slider space-top-40">
            <div><img src="<?php echo TEMPLATE_DIR_URI; ?>/images/game-feature-mobile-1.jpg" /></div>
            <div><img src="<?php echo TEMPLATE_DIR_URI; ?>/images/game-feature-mobile-2.jpg" /></div>
            <div><img src="<?php echo TEMPLATE_DIR_URI; ?>/images/game-feature-mobile-3.jpg" /></div>
            <div><img src="<?php echo TEMPLATE_DIR_URI; ?>/images/game-feature-mobile-4.jpg" /></div>
            <div><img src="<?php echo TEMPLATE_DIR_URI; ?>/images/game-feature-mobile-5.jpg" /></div>
        </div>
    </div>

    <!--影音圖集-->
<?php
$args = array(
    'post_type'      => 'multimedia',
    'post_status' => 'publish',
    'posts_per_page' => 6,
);


$video_query = new WP_Query( $args );
?>
    <div class="container space-top-50 hidden-xs">
        <div class="row">
            <div class="col-sm-12">
                <h3 class="text-center"><img src="<?php echo TEMPLATE_DIR_URI; ?>/images/heading-xs-media.png" /></h3>
                <div class="media-list">
                    <ul class="space-top-30">
<?php
while ( $video_query->have_posts() ) :
  $video_query->the_post();
  $ID = get_the_ID();

  $is_video = true;
  $mfp_class = 'mfp-iframe';
  $video_url = get_post_meta( $ID, POST_META_VIDEO, true );

  if( empty( $video_url ) ) {
    $video_url = get_the_post_thumbnail_url( $ID, 'full');
    $mfp_class = 'mfp-image';
    $is_video = false;
  };

  $thumb_nail = get_the_post_thumbnail_url( $ID, 'video-cover');

?>
                        <li class="media-list-item">
                            <a class="desktop-video video-cover <?php echo $mfp_class; ?>" href="<?php echo $video_url ?>" style="background-image:url('<?php echo $thumb_nail ?>')">
                            <?php if($is_video): ?>
                                <i class="fa fa-play-circle" aria-hidden="true"></i>
                            <?php endif; ?>
                            </a>
                            <div class="video-title"><?php the_title(); ?></div>
                        </li>
<?php
  endwhile;
?>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="space-top-40  space-bottom-80  hidden-xs">
        <div class="text-center">
            <a href="<?php echo home_url('/media/'); ?>" class="btn btn-lg btn-primary submit-btn">更多影片 <i class="fa fa-chevron-right" aria-hidden="true"></i></a>
        </div>
    </div>

    <!--影音圖集 手機版-->
    <div class="landing-media-mobile space-top-20  visible-xs-block">
        <h3 class="text-center mobile-heading"><img src="<?php echo TEMPLATE_DIR_URI; ?>/images/heading-xs-media.png" /></h3>

        <div class="landing-media-mobile-slider space-top-30">
<?php
while ( $video_query->have_posts() ) :
  $video_query->the_post();
  $ID = get_the_ID();

  $is_video = true;
  $mfp_class = 'mfp-iframe';
  $video_url = get_post_meta( $ID, POST_META_VIDEO, true );

  if( empty( $video_url ) ) {
    $video_url = get_the_post_thumbnail_url( $ID, 'full');
    $mfp_class = 'mfp-image';
    $is_video = false;
  };

  $thumb_nail = get_the_post_thumbnail_url( $ID, 'video-cover');

?>
            <div class="media-list-item">
                <a class="mobile-video video-cover <?php echo $mfp_class; ?>" href="<?php echo $video_url ?>" style="background-image:url('<?php echo $thumb_nail; ?>')">
                <?php if($is_video): ?>
                    <i class="fa fa-play-circle" aria-hidden="true"></i>
                <?php endif; ?>
                </a>
                <div class="video-title"><?php the_title(); ?></div>
            </div>
<?php
  endwhile;
?>
        </div>


        <div class="space-top-70  space-bottom-10  text-center">
            <a href="<?php echo home_url('/media/'); ?>" class="btn btn-lg btn-primary submit-btn">更多影片 <i class="fa fa-chevron-right" aria-hidden="true"></i></a>
        </div>

    </div>

    <div class="game-info-mobile space-top-30 visible-xs-block">

        <a href="<?php echo GAMER_GROUP_URL; ?>">
            <img src="<?php echo TEMPLATE_DIR_URI; ?>/images/banner-gamer.png" />
        </a>
        <a href="<?php echo FB_PAGE_URL; ?>">
            <img src="<?php echo TEMPLATE_DIR_URI; ?>/images/banner-facebook.png" />
        </a>
    </div>


<?php

wp_reset_postdata();

get_footer();
