<?php

// add action: nextend_fb_user_registered
function get_fb_business_id( $ID, $user_profile, $fb ){

  $businessToken = get_user_meta($ID, 'ids_for_business', true);
  if( empty($businessToken) ){

    $response = $fb->get('/me/ids_for_business');
    update_user_meta($ID, 'ids_for_business', json_decode($response));

  }

}
add_action('nextend_fb_user_registered', 'get_fb_business_id', 10, 3);
add_action('nextend_fb_user_logged_in', 'get_fb_business_id', 10, 3);
add_action('nextend_fb_user_account_linked', 'get_fb_business_id', 10, 3);

