<?php

function dragon_nest_tw_register_required_plugins() {
	$plugins = array(

		array(
			'name'      => 'Nextend Facebook Connect',
			'slug'      => 'nextend-facebook-connect',
			'required'  => true,
		),
		array(
			'name'      => 'Nextend Google Connect',
			'slug'      => 'nextend-google-connect',
			'required'  => true,
		),
		array(
			'name'      => 'Disable notification',
			'slug'      => 'disable-notification',
			'source'    => get_stylesheet_directory() . '/plugins/disable-wp-new-user-notification.zip',
			'required'  => true,
		),
		array(
			'name'      => 'Category Order and Taxonomy Terms Order',
			'slug'      => 'taxonomy-terms-order',
			'required'  => true,
		),
	);

	$config = array(
		'id'           => 'dragon_nest_tw',                 // Unique ID for hashing notices for multiple instances of TGMPA.
		'default_path' => '',                      // Default absolute path to bundled plugins.
		'menu'         => 'tgmpa-install-plugins', // Menu slug.
		'parent_slug'  => 'themes.php',            // Parent menu slug.
		'capability'   => 'edit_theme_options',    // Capability needed to view plugin install page, should be a capability associated with the parent menu used.
		'has_notices'  => true,                    // Show admin notices or not.
		'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
		'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
		'is_automatic' => false,                   // Automatically activate plugins after installation or not.
		'message'      => '',                      // Message to output right before the plugins table.
	);

	tgmpa( $plugins, $config );
}
add_action( 'tgmpa_register', 'dragon_nest_tw_register_required_plugins' );

