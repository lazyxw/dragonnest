<?php

define( 'GUILD_MEMBER_LIMIT', 500 );

define('TEMPLATE_DIR_URI', '/wp-content/themes/dragon_nest_tw');

define('FB_PAGE_URL', 'https://www.facebook.com/DragonNestMobile.TW/');
define('GAMER_GROUP_URL', 'https://forum.gamer.com.tw/A.php?bsn=30766');
define('QRCODE_IMG', 'https://dn.funplusgame.com/wp-content/uploads/2017/09/20170912-安卓二维码.gif');
define('STATIC_SITE_VIDEO_1', 'https://www.youtube.com/watch?v=4OHanxA8RKQ');
define('STATIC_SITE_VIDEO_1_TITLE', '《龍之谷手遊》事前登錄 血多動畫 敬請期待');
define('STATIC_SITE_VIDEO_2', 'https://www.youtube.com/watch?v=zeEBjmu2txQ');
define('STATIC_SITE_VIDEO_2_TITLE', '《龍之谷手遊》蛋事超有事 完美還原 廠商VS客戶');

// custom table
define('DN_USER_TABLE', 'dn_user');

// post meta
define('POST_META_VIDEO', 'youtube_url');
define('GUILD_META_COUNT', 'guild_member_count');

// user meta
define('USER_META_PHONE_NUMBER', 'phone_number');
define('USER_META_GUILD_ID', 'guild_id');
define('USER_META_VOTE_VIDEO', 'vote_video');

// option
define('SITE_USER_COUNT', 'site_user_count');
define('SITE_USER_OFFSET', 'site_user_offset');
define('SITE_PRE_REG_DUEDAY', 'site_pre_reg_dueday');
define('SITE_DOWNLOAD_IOS', 'site_download_ios');
define('SITE_DOWNLOAD_ANDROID', 'site_download_android');
define('SITE_DOWNLOAD_OFFICIAL', 'site_download_official');
define('SITE_VIDEO_HOME', 'site_video_home');
define('SITE_VIDEO_HOME_THUMB', 'site_video_home_thumb');
define('SITE_VIDEO_1', 'site_video_1');
define('SITE_VIDEO_1_TITLE', 'site_video_1_title');
define('SITE_VIDEO_2', 'site_video_2');
define('SITE_VIDEO_2_TITLE', 'site_video_2_title');
