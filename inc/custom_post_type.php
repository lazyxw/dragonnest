<?php

// change post's label to 最新消息
function change_post_menu_label() {
    global $menu;
    global $submenu;
    $menu[5][0] = '最新消息';
    $submenu['edit.php'][5][0] = '全部最新消息';
    $submenu['edit.php'][10][0] = '新增最新消息';
    echo '';
}

function change_post_object_label() {
        global $wp_post_types;
        $labels = &$wp_post_types['post']->labels;
        $labels->name = '最新消息';
        $labels->singular_name = '最新消息';
        $labels->add_new = '新增最新消息';
        $labels->add_new_item = '新增最新消息';
        $labels->edit_item = '編輯最新消息';
        $labels->new_item = '最新消息';
        $labels->view_item = '檢視最新消息';
        $labels->search_items = '搜尋最新消息';
        $labels->not_found = '找不到公會';
        $labels->not_found_in_trash = '垃圾桶找不到公會';
    }
add_action( 'init', 'change_post_object_label' );
add_action( 'admin_menu', 'change_post_menu_label' );


// register custom post: guild
function post_type_guild() {
  $labels = array(
      'name' => __( '公會' ),
      'singular_name' => 'guild',
      'add_new' => _x('新增', 'guild'),
      'add_new_item' => __('新增公會'),
      'edit_item' => __('編輯公會'),
      'new_item' => __('新公會'),
      'view_item' => __('檢視公會'),
      'search_items' => __('搜尋公會'),
      'not_found' =>  __('找不到公會'),
      'not_found_in_trash' => __('垃圾桶找不到公會'),
  );

  $args = array(
      'labels' => $labels,
      'public' => true,
      'has_archive' => false,
      'menu_position' => 9,
      'menu_icon' => 'dashicons-groups',
      'supports' => array('title'),
    );

  register_post_type( 'guild' , $args );
}
add_action( 'init', 'post_type_guild' );

// Add the custom columns to the book post type:
function set_custom_edit_guild_columns($columns) {
    $columns['id'] = 'ID';
    $columns['member_count'] = '成員數量';
    $columns['member_list'] = '成員列表';

    return $columns;
}
add_filter( 'manage_guild_posts_columns', 'set_custom_edit_guild_columns' );

// Add the data to the custom columns for the book post type:
function custom_guild_column( $column, $post_id ) {
  switch ( $column ) {
    case 'id' :
      echo $post_id;
      break;
    case 'member_count' :
      echo get_post_meta($post_id, GUILD_META_COUNT, true);
      break;

    case 'member_list' :
        echo '<a href="admin-ajax.php?action=get_guild_members&listall&g=' . $post_id . '" target="_blank">顯示</a>';
        break;

  }
}
add_action( 'manage_guild_posts_custom_column' , 'custom_guild_column', 10, 2 );


function addCustomImportButton() {
    global $current_screen;

    if ('guild' != $current_screen->post_type) {
        return;
    }

    ?>
        <script type="text/javascript">
            jQuery(document).ready( function($)
            {
                jQuery(jQuery(".wrap h1")[0]).append("<a class='add-new-h2' href='admin-ajax.php?action=export_guilds' target='_blank'>匯出所有資料</a>");
            });
        </script>
    <?php
}
add_action('admin_head-edit.php','addCustomImportButton');


// register custom post: multimedia
function post_type_multimedia() {
  $labels = array(
      'name' => __( '影音圖集' ),
      'singular_name' => 'multimedia',
      'add_new' => _x('新增', 'multimedia'),
      'add_new_item' => __('新增影音圖集'),
      'edit_item' => __('編輯影音圖集'),
      'new_item' => __('新影音圖集'),
      'view_item' => __('檢視影音圖集'),
      'search_items' => __('搜尋影音圖集'),
      'not_found' =>  __('找不到影音圖集'),
      'not_found_in_trash' => __('垃圾桶找不到影音圖集'),
  );

  $args = array(
      'labels' => $labels,
      'public' => true,
      'has_archive' => false,
      'menu_position' => 5,
      'menu_icon' => 'dashicons-format-video',
      'supports' => array('title', 'thumbnail'),
    );

  register_post_type( 'multimedia' , $args );
}
add_action( 'init', 'post_type_multimedia' );

//create a custom taxonomy name it topics for your posts
function create_media_cat_hierarchical_taxonomy() {
  $labels = array(
    'name' => '影音分類',
    'singular_name' => '影音分類',
    'menu_name' => __( '影音分類' ),
  );

  register_taxonomy('media_cat', array('multimedia'), array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'show_admin_column' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'media_cat' ),
  ));

}
add_action( 'init', 'create_media_cat_hierarchical_taxonomy', 0 );

// register meta box: video for multimedia
class MutileMedia_Video_Meta_Box {

	public function __construct() {
		if ( is_admin() ) {
			add_action( 'load-post.php',     array( $this, 'init_metabox' ) );
			add_action( 'load-post-new.php', array( $this, 'init_metabox' ) );
		}
	}

	public function init_metabox() {
		add_action( 'add_meta_boxes', array( $this, 'add_metabox'  )        );
		add_action( 'save_post',      array( $this, 'save_metabox' ), 10, 2 );
	}

	public function add_metabox() {
		add_meta_box(
			'video',
			'影片網址',
			array( $this, 'render_metabox' ),
			'multimedia',
			'normal',
			'default'
		);
	}

	public function render_metabox( $post ) {

		// Retrieve an existing value from the database.
		$video_url = get_post_meta( $post->ID, POST_META_VIDEO, true );

		// Set default values.
		if( empty( $video ) ) $video = '';

		// Form fields.
		echo '<table class="form-table">';

		echo '	<tr>';
		echo '		<th><label for="' . POST_META_VIDEO . '" class="video_url_label">影片網址</label></th>';
		echo '		<td>';
		echo '			<input id="' . POST_META_VIDEO . '" style="width:100%" name="' . POST_META_VIDEO . '" class="videoe_field" value="' . esc_attr__( $video_url ) . '">';
		echo '			<p class="description">未輸入影片網址時，前端僅顯示精選圖片</p>';
		echo '		</td>';
		echo '	</tr>';
		echo '</table>';

	}

	public function save_metabox( $post_id, $post ) {

		// Check if the user has permissions to save data.
		if ( ! current_user_can( 'edit_post', $post_id ) )
			return;

		// Check if it's not an autosave.
		if ( wp_is_post_autosave( $post_id ) )
			return;

		// Check if it's not a revision.
		if ( wp_is_post_revision( $post_id ) )
			return;

		// Sanitize user input.
		$video_url = isset( $_POST[ POST_META_VIDEO ] ) ? sanitize_text_field( $_POST[ POST_META_VIDEO ] ) : '';

		// Update the meta field in the database.
		update_post_meta( $post_id, POST_META_VIDEO, $video_url );

    if( ! empty( $video_url ) ){
      preg_match('/^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/', $video_url, $matches);
      if ( isset($matches[7]) ){

          $youtube_thumb = "https://img.youtube.com/vi/" . $matches[7] . "/maxresdefault.jpg";

          $tmp = download_url( $youtube_thumb );

          // Set variables for storage
          // fix file filename for query strings
          preg_match('/[^\?]+\.(jpg|JPG|jpe|JPE|jpeg|JPEG|gif|GIF|png|PNG)/', $youtube_thumb, $matches);
          $file_array['name'] = basename($matches[0]);
          $file_array['tmp_name'] = $tmp;

          // If error storing temporarily, unlink
          if ( is_wp_error( $tmp ) ) {
              @unlink($file_array['tmp_name']);
              $file_array['tmp_name'] = '';
          }

          // do the validation and storage stuff
          $thumbid = media_handle_sideload( $file_array, $post_id);
          // If error storing permanently, unlink
          if ( is_wp_error($thumbid) ) {
              @unlink($file_array['tmp_name']);
          } else {
            set_post_thumbnail($post_id, $thumbid);
          }
      }
    }


	}

}

new MutileMedia_Video_Meta_Box;