<?php


// search guild
function get_guild_list(){

  if( ! is_user_logged_in() ) die();

  $args = array(
      'post_type'      => 'guild', // taxonomy name
      'orderby'       => 'ID',
      'order'         => 'ASC',
      'post_status'    => 'publish',
      'nopaging'  => true,
  );


  if( isset($_POST['s']) ) {
    $search_term = sanitize_text_field($_POST['s']);
    if( ! empty($search_term) ){
      if( is_numeric($search_term) ) {
        $args = array_merge( $args, array(
          'p'    => $search_term,
        ));
      } else {
        $args = array_merge( $args, array(
          's'    => $search_term,
        ));
      }
    };
  }


  $posts = get_posts( $args );

  $guilds = [];
  foreach( $posts as $guild ){
    $mcount = get_post_meta($guild->ID, GUILD_META_COUNT, true);
    if( $mcount < GUILD_MEMBER_LIMIT ) {
      if( isset($_POST['s']) ){
        $title = $guild->post_title;
      } else {
        $title = '[' . $guild->ID . '] ' . $guild->post_title  . " ( " . $mcount . "人 )";
      }

    } else {
      if( isset($_POST['s']) ){
        $title = $guild->post_title;
      } else {
        $title = '[' . $guild->ID . '] ' . $guild->post_title  . " ( 額滿 )";
      }

    }

    $guilds[] = array(
      'id' => $guild->ID,
      'title' => $title,
    );
  }
  echo json_encode($guilds);

	die();
}
add_action('wp_ajax_get_guild_list', 'get_guild_list');


// list members for dedicated guild
function get_guild_members() {
  if( ! is_user_logged_in() ) die();

  if( isset($_REQUEST['g']) && is_numeric($_REQUEST['g']) ) {
    $guild_id = $_REQUEST['g'];
  } else {
    die();
  }
  if( empty($guild_id) ) die();

  $args = array(
    // 'role' => 'subscriber',
    'meta_key' => USER_META_GUILD_ID,
    'meta_value' => $guild_id,
    'orderby' => 'display_name',
    'fields' => array('display_name'),
  );

  // if( isset($_REQUEST['listall']) ){
  //   unset($args['number']);
  //   unset($args['paged']);
  // }
  $users = get_users( $args );

  $result = [];
  foreach( $users as $u ) {
    $result[] = $u->display_name;
  }

  if( isset($_REQUEST['listall']) ){
    if( is_array($result) ){
      echo implode( "\n", $result );
    } else {
      echo 'no member';
    }
  } else {
    echo json_encode($result);
  }


	die();
}
add_action('wp_ajax_get_guild_members', 'get_guild_members');

function save_phone() {
  global $wpdb;

  if( ! is_user_logged_in() || ! isset($_POST['phone']) ) die();

  $phone = sanitize_text_field($_POST['phone']);
  if( empty($phone) ) die();

  $user_data = wp_get_current_user()->data;
  if( ! empty( get_user_meta($user_data->ID, USER_META_PHONE_NUMBER, true) ) ) die();

  $check = $wpdb->get_var( $wpdb->prepare( "SELECT COUNT(umeta_id) FROM $wpdb->usermeta WHERE meta_key = %s AND meta_value = %s", USER_META_PHONE_NUMBER, $phone) );

  if( $check >= 1 ){
    echo -2;
    die();
  }


  $result = update_user_meta($user_data->ID, USER_META_PHONE_NUMBER, $phone);
  if( $result === false ) die();

  $dn_user = array(
    'ID' => $user_data->ID,
    'phone_number' => $phone,
  );
  update_dn_user($dn_user);

  echo 1;
  die();
}
add_action('wp_ajax_save_phone', 'save_phone');

// save guild
function save_guild() {
  global $wpdb;

  if( ! is_user_logged_in() || ! isset($_POST['guild']) || ! isset($_POST['nickname']) ) die();

  $guild_name = sanitize_text_field($_POST['guild']);
  if( empty($guild_name) ) die();

  $nickname = sanitize_text_field($_POST['nickname']);
  if( empty($nickname) ) die();

  $user_data = wp_get_current_user()->data;
  if( ! empty( get_user_meta($user_data->ID, USER_META_GUILD_ID, true) ) ) die();

  $guild = get_page_by_title($guild_name, OBJECT, 'guild');

  if( empty($guild->ID) ){
    $postarr = array(
      'post_type' => 'guild',
      'post_title' => $guild_name,
      'post_author' => $user_data->ID,
      'post_status' => 'publish',
    );

    // create guild if not exist
    $guild_id = wp_insert_post($postarr);
  } else {
    $guild_id = $guild->ID;

    // check member count
    $member_count = get_post_meta($guild_id, GUILD_META_COUNT, true);
    if( $member_count >= GUILD_MEMBER_LIMIT ){
      echo -1;
      die();
    }
  }

  // check user's display name

  $check = $wpdb->get_var( $wpdb->prepare( "SELECT COUNT(ID) FROM $wpdb->users WHERE display_name = %s", $nickname) );
  // echo $wpdb->prepare( "SELECT COUNT(ID) FROM $wpdb->users WHERE display_name = %s", $nickname);
  // echo $nickname;
  // echo $user_data->ID;
  // echo $check;
  // die();
  if( $check >= 1 ){
    echo -2;
    die();
  }

  // save user's nickname
  $user_data->display_name = $nickname;
  $result = wp_update_user($user_data);
  if(is_wp_error($result)) die();

  // save user's guild
  $result = update_user_meta($user_data->ID, USER_META_GUILD_ID, $guild_id);
  if( $result === false ) die();

  // count guild's members
  $args = array(
    'meta_key' => USER_META_GUILD_ID,
    'meta_value' => $guild_id,
  );

  $user_query = new WP_User_Query( $args );
  update_post_meta($guild_id, GUILD_META_COUNT, $user_query->total_users);

  // save data to dn_user
  $dn_user = array(
    'ID' => $user_data->ID,
    'display_name' => $nickname,
    'guild' => $guild_id,
  );
  update_dn_user($dn_user);

  echo 1;
	die();
}
add_action('wp_ajax_save_guild', 'save_guild');


// vote video
function vote_video() {
  if( ! is_user_logged_in() || ! isset($_POST['v']) ) die();

  $video = sanitize_text_field($_POST['v']);
  if( empty($video) ) die();
  if( ! in_array($video, array(1, 2) ) ) die();

  $user_data = wp_get_current_user()->data;
  if( ! empty( get_user_meta($user_data->ID, USER_META_VOTE_VIDEO, true) ) ) die();

  $result = update_user_meta($user_data->ID, USER_META_VOTE_VIDEO, $video);
  if( $result === false ) die();

  $dn_user = array(
    'ID' => $user_data->ID,
    'voted_video' => $video,
  );
  update_dn_user($dn_user);

  echo 1;
	die();
}
add_action('wp_ajax_vote_video', 'vote_video');



// export dn user data
function export_guilds() {
  if( ! current_user_can('administrator') ) die();

  header("Pragma: public");
  header("Expires: 0");
  header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
  header("Cache-Control: private", false);
  header("Content-Type: application/octet-stream");
  header("Content-Disposition: attachment; filename=\"dn_guild_" . time() . ".csv\";" );
  header("Content-Transfer-Encoding: binary");

  $content = "公會序號\t創建時間\t公會名稱\t成員數量\t成員列表\n";

  $args = array(
      'post_type'      => 'guild', // taxonomy name
      'orderby'       => 'ID',
      'order'         => 'ASC',
      'post_status'    => 'publish',
      'nopaging' => true,
  );

  $posts = get_posts( $args );

  foreach( $posts as $guild ){
    $args = array(
      'meta_key' => USER_META_GUILD_ID,
      'meta_value' => $guild->ID,
      'orderby' => 'display_name',
      'fields' => array('display_name'),
      'count_total' => true,
    );
    $users = new WP_User_Query( $args );
    $member_list = [];

    if ( ! empty( $users->results ) ) {
      foreach( $users->results as $u ) {
        $member_list[] = '"' . $u->display_name . '"';
      }
    }

    $content .= $guild->ID . "\t";
    $content .= $guild->post_date . "\t";
    $content .= $guild->post_title . "\t";
    $content .= $users->get_total() . "\t";
    $content .= implode(',', $member_list) . "\t";

    $content .= "\n";
  }

  echo $content;
  die();
}
add_action('wp_ajax_export_guilds', 'export_guilds');


// export dn user data
function export_dn_users() {
  if( ! current_user_can('administrator') ) die();

  header("Pragma: public");
  header("Expires: 0");
  header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
  header("Cache-Control: private", false);
  header("Content-Type: application/octet-stream");
  header("Content-Disposition: attachment; filename=\"dn_prereg_" . time() . ".csv\";" );
  header("Content-Transfer-Encoding: binary");

  $content = "註冊序號\t註冊時間\t註冊Email\t手機號碼\t公會序號\t投票記錄\t來源\tCLIENT ID\tTOKEN FOR BUSINESS\n";

  global $wpdb;

  $tbl_name = $wpdb->prefix . DN_USER_TABLE;
  $sql = "SELECT * FROM $tbl_name order by ID";

  $result = $wpdb->get_results( $sql, 'ARRAY_A' );

  foreach($result as $dn_user) {
    $content .= $dn_user['ID'] . "\t";
    $content .= get_date_from_gmt( $dn_user['user_registered'] ) . "\t";
    $content .= $dn_user['user_email'] . "\t";
    $content .= $dn_user['phone_number'] . "\t";
    $content .= $dn_user['guild'] . "\t";
    $content .= $dn_user['voted_video'] . "\t";
    $content .= $dn_user['login_type'] . "\t";
    $content .= $dn_user['client_id'] . "\t";
    $content .= $dn_user['token_for_business'] . "\t";
    $content .= "\n";
  }

  echo $content;
  die();
}
add_action('wp_ajax_export_dn_users', 'export_dn_users');
