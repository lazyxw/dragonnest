<?php

function theme_settings_page(){
?>
  <div class="wrap">
    <h1>官網設定</h1>
    <form method="post" action="options.php">
        <?php
            settings_fields("section");
            do_settings_sections("theme-options");
            submit_button();
        ?>
    </form>
  </div>
<?php
}

function add_theme_menu_item(){
	add_menu_page("官網設定", "官網設定", "manage_options", "theme-panel", "theme_settings_page", null, 99);
}
add_action("admin_menu", "add_theme_menu_item");


function display_dueday_element(){
	?>
    	<input type="text" name="<?php echo SITE_PRE_REG_DUEDAY; ?>" id="<?php echo SITE_PRE_REG_DUEDAY; ?>" value="<?php echo get_option(SITE_PRE_REG_DUEDAY); ?>" /> <span class="description">格式： YYYY/MM/DD</span>
    <?php
}


function display_reg_offset_element(){
	?>
    	<input type="text" name="<?php echo SITE_USER_OFFSET; ?>" id="<?php echo SITE_USER_OFFSET; ?>" value="<?php echo get_option(SITE_USER_OFFSET); ?>" /> <span class="description">網頁顯示的預註冊人數會是實際人數 + 預註冊人數修正</span>
    <?php
}


function display_download_ios_element(){
	?>
    	<input type="text" name="<?php echo SITE_DOWNLOAD_IOS; ?>" id="<?php echo SITE_DOWNLOAD_IOS; ?>" value="<?php echo get_option(SITE_DOWNLOAD_IOS); ?>" size="50" /> <span class="description">App Store 下載連結</span>
    <?php
}


function display_download_android_element(){
	?>
    	<input type="text" name="<?php echo SITE_DOWNLOAD_ANDROID; ?>" id="<?php echo SITE_DOWNLOAD_ANDROID; ?>" value="<?php echo get_option(SITE_DOWNLOAD_ANDROID); ?>" size="50" /> <span class="description">Google Play 下載連結</span>
    <?php
}


function display_download_official_element(){
	?>
    	<input type="text" name="<?php echo SITE_DOWNLOAD_OFFICIAL; ?>" id="<?php echo SITE_DOWNLOAD_OFFICIAL; ?>" value="<?php echo get_option(SITE_DOWNLOAD_OFFICIAL); ?>" size="50" /> <span class="description">官方下載連結</span>
    <?php
}


function display_video_home_element(){
	?>
    	<input type="text" name="<?php echo SITE_VIDEO_HOME; ?>" id="<?php echo SITE_VIDEO_1; ?>" value="<?php echo get_option(SITE_VIDEO_HOME); ?>" size="50" /> <span class="description">請輸入首頁影片介紹的 youtube 網址</span>
    <?php
}

function display_video_home_thumb_element(){
    wp_enqueue_media();

    $thumbnail = get_option(SITE_VIDEO_HOME_THUMB);
    if( ! empty($thumbnail) ):
	?>
    <img class="img-sponsor-logo" src="<?php echo $thumbnail; ?>" style="max-width: 200px;" /><br/>
    <?php endif; ?>
    <input type="text" name="<?php echo SITE_VIDEO_HOME_THUMB; ?>" id="<?php echo SITE_VIDEO_HOME_THUMB; ?>" value="<?php echo $thumbnail; ?>" size="50" /> <span class="description">請輸入首頁影片介紹的縮圖(330x150)</span><br />
    <button class="z_upload_logo_button button">上傳縮圖</button>
    <button class="z_remove_logo_button button">移除縮圖</button>
    <script>
        var file_frame;

        jQuery(document).ready(function($) {
            $(".z_upload_logo_button").click(function(event) {
                upload_button = $(this);
                var frame;
                event.preventDefault();
                if (frame) {
                    frame.open();
                    return;
                }
                frame = wp.media();
                frame.on( "select", function() {
                // Grab the selected attachment.
                var attachment = frame.state().get("selection").first();
                frame.close();
                if (upload_button.parent().prev().children().hasClass("tax_list")) {
                    upload_button.parent().prev().children().val(attachment.attributes.url);
                    upload_button.parent().prev().prev().children().attr("src", attachment.attributes.url);
                }
                else
                    $("#<?php echo SITE_VIDEO_HOME_THUMB; ?>").val(attachment.attributes.url);
                });
                frame.open();
            });

            $(".z_remove_logo_button").click(function() {
                $(".img-sponsor-logo").attr("src", "");
                $("#<?php echo SITE_VIDEO_HOME_THUMB; ?>").val("");
                $(this).parent().siblings(".title").children("img").attr("src","");
                $(".inline-edit-col :input[name=\'<?php echo SITE_VIDEO_HOME_THUMB; ?>\']").val("");
                return false;
            });
        });
    </script>
    <?php
}

function display_video1_element(){
	?>
    	<input type="text" name="<?php echo SITE_VIDEO_1; ?>" id="<?php echo SITE_VIDEO_1; ?>" value="<?php echo get_option(SITE_VIDEO_1); ?>" size="50" /> <span class="description">請輸入投票影片1 youtube 網址</span>
    <?php
}

function display_video2_element(){
	?>
    	<input type="text" name="<?php echo SITE_VIDEO_2; ?>" id="<?php echo SITE_VIDEO_2; ?>" value="<?php echo get_option(SITE_VIDEO_2); ?>" size="50" /> <span class="description">請輸入投票影片2 youtube 網址</span>
    <?php
}

function display_video1_title_element(){
	?>
    	<input type="text" name="<?php echo SITE_VIDEO_1_TITLE; ?>" id="<?php echo SITE_VIDEO_1_TITLE; ?>" value="<?php echo get_option(SITE_VIDEO_1_TITLE); ?>" size="50" /> <span class="description">請輸入投票影片1標題</span>
    <?php
}

function display_video2_title_element(){
	?>
    	<input type="text" name="<?php echo SITE_VIDEO_2_TITLE; ?>" id="<?php echo SITE_VIDEO_2_TITLE; ?>" value="<?php echo get_option(SITE_VIDEO_2_TITLE); ?>" size="50" /> <span class="description">請輸入投票影片2標題</span>
    <?php
}

function display_theme_panel_fields(){

	add_settings_section("section", "事前註冊", null, "theme-options");

	add_settings_field(SITE_PRE_REG_DUEDAY, "截止日期", "display_dueday_element", "theme-options", "section");
	add_settings_field(SITE_USER_OFFSET, "預註冊人數修正", "display_reg_offset_element", "theme-options", "section");
	add_settings_field(SITE_DOWNLOAD_IOS, "App Store 下載連結", "display_download_ios_element", "theme-options", "section");
	add_settings_field(SITE_DOWNLOAD_ANDROID, "Google Play 下載連結", "display_download_android_element", "theme-options", "section");
	add_settings_field(SITE_DOWNLOAD_OFFICIAL, "官方下載連結", "display_download_official_element", "theme-options", "section");
	add_settings_field(SITE_VIDEO_HOME, "首頁影片網址", "display_video_home_element", "theme-options", "section");
	add_settings_field(SITE_VIDEO_HOME_THUMB, "首頁影片縮圖", "display_video_home_thumb_element", "theme-options", "section");
	add_settings_field(SITE_VIDEO_1, "投票影片網址 1", "display_video1_element", "theme-options", "section");
	add_settings_field(SITE_VIDEO_2, "投票影片網址 2", "display_video2_element", "theme-options", "section");

	add_settings_field(SITE_VIDEO_1_TITLE, "投票影片標題 1", "display_video1_title_element", "theme-options", "section");
	add_settings_field(SITE_VIDEO_2_TITLE, "投票影片標題 2", "display_video2_title_element", "theme-options", "section");

	// add_settings_field(SITE_VIDEO_1, "投票影片縮圖 1", "display_video1_thumb_element", "theme-options", "section");
	// add_settings_field(SITE_VIDEO_2, "投票影片縮圖 2", "display_video2_thumb_element", "theme-options", "section");

    register_setting("section", SITE_PRE_REG_DUEDAY);
    register_setting("section", SITE_USER_OFFSET);
    register_setting("section", SITE_DOWNLOAD_IOS);
    register_setting("section", SITE_DOWNLOAD_ANDROID);
    register_setting("section", SITE_DOWNLOAD_OFFICIAL);
    register_setting("section", SITE_VIDEO_HOME);
    register_setting("section", SITE_VIDEO_HOME_THUMB);
    register_setting("section", SITE_VIDEO_1);
    register_setting("section", SITE_VIDEO_2);
    register_setting("section", SITE_VIDEO_1_TITLE);
    register_setting("section", SITE_VIDEO_2_TITLE);
//   register_setting("section", SITE_VIDEO_1_THUMB);
//   register_setting("section", SITE_VIDEO_2_THUMB);
}
add_action("admin_init", "display_theme_panel_fields");