<?php

/**
 * 建立資料表
 */
function create_custom_table() {
  require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

  global $wpdb;
  $charset_collate = $wpdb->get_charset_collate();

  $tmp_name = $wpdb->prefix . DN_USER_TABLE;
  $sql = "CREATE TABLE IF NOT EXISTS " . $tmp_name . " (
    ID bigint(20) NOT NULL,
    user_registered datetime NOT NULL DEFAULT NOW(),
    user_email varchar(100) NOT NULL,
    display_name varchar(250) NOT NULL,
    phone_number varchar(10) DEFAULT '',
    guild int(11) DEFAULT 0,
    voted_video int(11) DEFAULT 0,
    login_type varchar(100) DEFAULT '',
    client_id varchar(100) DEFAULT '',
    token_for_business varchar(250) DEFAULT '',
    PRIMARY KEY  (ID)
  ) $charset_collate;";
  dbDelta($sql);
}
add_action('after_switch_theme', 'create_custom_table');


// // update total registered user
function calculate_user_count() {
  // $usercount = count_users();

  // if( isset($usercount['avail_roles']['subscriber']) ) {
  //   $count = $usercount['avail_roles']['subscriber'] + 1;
  // } else {
  //   $count = 0;
  // }
  global $wpdb;

  $tmp_name = $wpdb->prefix . DN_USER_TABLE;

  $count = $wpdb->get_var( "SELECT COUNT(ID) FROM $tmp_name " );

  update_option( SITE_USER_COUNT, $count);
}
add_action( 'user_register', 'calculate_user_count', 10, 1 );


// add dn user menu
function dn_user_admin_menu() {
    add_users_page('事前登錄名單', '事前登錄名單', 'manage_options', 'pre-reg-list', 'dn_user_admin_handler');
}
add_action('admin_menu', 'dn_user_admin_menu');

// handler for dn user admin
function dn_user_admin_handler() {
    $table = new DN_User_Table();
    $table->prepare_items();
?>
    <style>
        #ID{width:50px;}
        #guild{width:50px;}
        #voted_video{width:50px;}
    </style>
    <div class="wrap">
        <h2>事前登錄名單</h2>
        <br>
        <a class="page-title-action" href="admin-ajax.php?action=export_dn_users" target="_blank">匯出名單</a>
        <form method="get">
            <?php $table->display() ?>
        </form>

    </div>
<?php

}


// insert new register to dn user table
function add_dn_user( $user_login, $user ) {

  if ( in_array('subscriber', $user->roles)) {
    $dn_user = get_dn_user( $user->ID );

    if ( empty($dn_user) ){
      $dn_user = mapping_dn_user($user);
      insert_dn_user($dn_user);
    }
  }
  calculate_user_count();
}
add_action('wp_login', 'add_dn_user', 10, 2);

function mapping_dn_user ( $user ) {
  $dn_user = [];

  if ( ! empty( $user->ID) )  $dn_user['ID'] = $user->ID;
  if ( ! empty( $user->user_registered) )  $dn_user['user_registered'] = $user->user_registered;
  if ( ! empty( $user->user_email) )  $dn_user['user_email'] = $user->user_email;
  if ( ! empty( $user->display_name) )  $dn_user['display_name'] = $user->display_name;
  if ( ! empty( $user->phone_number) )  $dn_user['phone_number'] = $user->phone_number;
  if ( ! empty( $user->guild) )  $dn_user['guild'] = $user->guild;
  if ( ! empty( $user->voted_video) )  $dn_user['voted_video'] = $user->voted_video;

  if ( strpos($user->user_login, 'Google - ') !== false ) {
    $dn_user['login_type'] = 'google';
  } else if ( strpos($user->user_login, 'Facebook - ') !== false ) {
    $dn_user['login_type'] = 'facebook';
  }

  return $dn_user;
}

function get_dn_user( $ID ) {
  global $wpdb;

  $tmp_name = $wpdb->prefix . DN_USER_TABLE;
  $sql = $wpdb->prepare( "SELECT * FROM $tmp_name WHERE ID = %d ;", $ID );

  $dn_user = $wpdb->get_row( $sql );

  if( empty($dn_user->client_id) || empty($dn_user->login_type) ) {

    $tb_name = $wpdb->prefix . 'social_users';
    $sql = $wpdb->prepare( "SELECT * FROM $tb_name WHERE ID = %d ;", $ID );
    $social_user = $wpdb->get_row( $sql );

    if( ! empty( $social_user ) ) {

      if ( $social_user->type == 'fb' ) {
        $fb_info = get_dn_user_fb_info( $ID );

        if( ! empty( $fb_info ) ) {
          $user = array(
            'ID' => $ID,
            'client_id' => $fb_info->id,
            'login_type' => 'facebook',
            'token_for_business' => $fb_info->token_for_business,
          );

          update_dn_user($user);
        }
      } else if ( $social_user->type == 'google' ) {

        $user = array(
          'ID' => $ID,
          'login_type' => 'google',
          'client_id' => $social_user->identifier,
        );

        update_dn_user($user);
      }
    }
  }

  return $dn_user;
}


function insert_dn_user( $user ) {
  global $wpdb;
  $wpdb->replace( $wpdb->prefix . DN_USER_TABLE, $user);
}


function update_dn_user( $user ) {
  global $wpdb;

  $where = array( 'ID' => $user['ID'] );
  unset($user['ID']);
  $wpdb->update( $wpdb->prefix . DN_USER_TABLE, $user, $where);
}

function get_dn_user_fb_info( $user_id ) {
  if (!class_exists('Facebook')) {
      require(ABSPATH . 'wp-content/plugins/nextend-facebook-connect/Facebook/autoload.php');
  }

  $settings = maybe_unserialize(get_option('nextend_fb_connect'));

  if (defined('NEXTEND_FB_APP_ID')) $settings['fb_appid'] = NEXTEND_FB_APP_ID;
  if (defined('NEXTEND_FB_APP_SECRET')) $settings['fb_secret'] = NEXTEND_FB_APP_SECRET;

  $fb = new Facebook\Facebook(array(
      'app_id'                  => $settings['fb_appid'],
      'app_secret'              => $settings['fb_secret']
  ));

  $result = '';
  $access_token = new_fb_get_user_access_token($user_id);
  if( ! empty($access_token) ) {
    $response = $fb->get('/me?fields=token_for_business', $access_token);
    $result = json_decode($response->getBody());
  }
  return $result;
}
