<?php
if ( ! class_exists( 'WP_List_Table' ) ) {
	require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

class DN_User_Table extends WP_List_Table {
	function __construct() {
      parent::__construct(array(
          'singular' => 'dn_user',
          'plural' => 'dn_users',
      ));
  }

  public static function get_dn_users( $per_page = 100, $page_number = 1 ) {

    global $wpdb;

    $tbl_name = $wpdb->prefix . DN_USER_TABLE;
    $sql = "SELECT * FROM $tbl_name";

    if ( ! empty( $_REQUEST['orderby'] ) ) {
      $sql .= ' ORDER BY ' . esc_sql( $_REQUEST['orderby'] );
      $sql .= ! empty( $_REQUEST['order'] ) ? ' ' . esc_sql( $_REQUEST['order'] ) : ' ASC';
    }

    $sql .= " LIMIT $per_page";

    $sql .= ' OFFSET ' . ( $page_number - 1 ) * $per_page;


    $result = $wpdb->get_results( $sql, 'ARRAY_A' );
    return $result;
  }

  public static function record_count() {
    global $wpdb;

    $tbl_name = $wpdb->prefix . DN_USER_TABLE;

    $sql = "SELECT COUNT(*) FROM $tbl_name";

    return $wpdb->get_var( $sql );
  }

  public function no_items() {
    _e( '找不到資料');
  }

	/**
	 * Render a column when no column specific method exist.
	 *
	 * @param array $item
	 * @param string $column_name
	 *
	 * @return mixed
	 */
  function column_default( $item, $column_name ) {
    switch( $column_name ) {
      case 'ID':
      case 'user_email':
      case 'phone_number':
      case 'display_name':
      case 'guild':
      case 'voted_video':
      case 'login_type':
        return $item[ $column_name ];
      case 'user_registered':
        return get_date_from_gmt( $item[ $column_name ] ) ;
      default:
        return print_r( $item, true ) ; //Show the whole array for troubleshooting purposes
    }
  }

	/**
	 * Method for name column
	 *
	 * @param array $item an array of DB data
	 *
	 * @return string
	 */
	function column_name( $item ) {
		return $title;
	}


	/**
	 *  Associative array of columns
	 *
	 * @return array
	 */
	function get_columns() {
		$columns = [
      'ID'  => 'ID',
      'user_registered' => '註冊時間',
			'user_email' => 'email',
			'phone_number' => '手機',
			'display_name' => '暱稱',
			'guild' => '公會',
			'voted_video' => '投票',
      'login_type' => '登入來源'
		];

		return $columns;
	}


	/**
	 * Columns to make sortable.
	 *
	 * @return array
	 */
	public function get_sortable_columns() {
		$sortable_columns = array(
      'ID'  => array( 'ID', true ),
      'user_registered' => array( 'user_registered', false ),
			'user_email' => array( 'user_email', false ),
			'phone_number' => array( 'phone_number', false ),
			'display_name' => array( 'display_name', false ),
			'guild' => array( 'guild', true ),
			'voted_video' => array( 'voted_video', false ),
      'login_type' => array( 'login_type', false ),
		);

		return $sortable_columns;
	}


	/**
	 * Handles data query and filter, sorting, and pagination.
	 */
	public function prepare_items() {

		$this->_column_headers = $this->get_column_info();

		// /** Process bulk action */
		$this->process_bulk_action();

		$per_page     = $this->get_items_per_page( 'customers_per_page', 100 );
		$current_page = $this->get_pagenum();
		$total_items  = self::record_count();

		$this->set_pagination_args( [
			'total_items' => $total_items, //WE have to calculate the total number of items
			'per_page'    => $per_page //WE have to determine how many items to show on a page
		] );

    $columns = $this->get_columns();
    $hidden = array();
    $sortable = array();
    $this->_column_headers = array($columns, $hidden, $sortable);
    $this->items = self::get_dn_users( $per_page, $current_page );
	}

}
