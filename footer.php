<?php if( ! is_front_page() ): ?>
    </div>
<?php endif; ?>

    <footer class="container ">
        <div class="row ">
            <div class="col-md-6 col-sm-6 col-xs-12  text-right">
                    <img src="<?php echo TEMPLATE_DIR_URI; ?>/images/12age-banner.png" alt="" />
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <span>本遊戲內容涉及暴力與清涼服飾。<br>
                        部分內容另外收費。注意使用時間.避免沉迷遊戲。 <br>
                        用戶協議：龍之谷使用者協議 個人咨詢及隱私保護政
                </span>
            </div>
        </div>
    </footer>

<script type="text/javascript ">
jQuery(document).ready(function($){
	$(function () {
		$('.hamburger-btn').click(function () {
			toggleNav();
		});

		$('.btn-close').click(function () {
			toggleNav();
		});
	});


	function toggleNav() {
		if ($('.mobile-main-menu').hasClass('show-nav')) {
			$('.mobile-main-menu').removeClass('show-nav');
		} else {
			$('.mobile-main-menu').addClass('show-nav');
		}
	}

	<?php if( is_front_page() ): ?>
	$('.landing-game-feature').slick({
		dots: true
	});

	$('.landing-classes-slider').slick({
		dots: true
	});

	$('.game-feature-mobile-slider').slick({
		dots: true
	});

	$('.landing-media-mobile-slider').slick({
		dots: true,
		arrows: false
	});

	$('a.desktop-video').magnificPopup({
		gallery: {
			enabled: true
		}
	});

	$('a.mobile-video, a.home-video').magnificPopup({
	});

	var selected_portrait = 0;
	$( ".landing-classes-list li" ).each(function( index ) {
		$(this).on('click', function(){
			$(".landing-classes-content img").eq(selected_portrait).toggleClass('active').removeClass('fadeInRight animated');
			$("div.landing-classes-info").eq(selected_portrait).toggleClass('active').removeClass('fadeInLeft animated');
			$(".landing-classes-list li").eq(selected_portrait).toggleClass('active');

			$(".landing-classes-content img").eq(index).toggleClass('active').addClass('fadeInRight animated');
			$("div.landing-classes-info").eq(index).toggleClass('active').addClass('fadeInLeft animated');
			$(".landing-classes-list li").eq(index).toggleClass('active');

			selected_portrait = index;
		});
	});

	<?php endif; ?>
});
</script>

<?php wp_footer(); ?>

</body>

</html>