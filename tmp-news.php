<?php
/*
 * Template name: news
 */

get_header();?>

        <div class="inner-page-block">
            <div class="inner-page-banner-img">
                <img src="<?php echo TEMPLATE_DIR_URI; ?>/images/header-banner-news.png" alt="" />
            </div>


            <div class="inner-content">
                <div class="decorative-borders-center"></div>

                <h2 class="heading text-center hidden-xs">
                    <img src="<?php echo TEMPLATE_DIR_URI; ?>/images/heading-lg-news.png" alt=""/>
                </h2>

                <h2 class="heading text-center visible-xs">
                    <img src="<?php echo TEMPLATE_DIR_URI; ?>/images/heading-xs-news.png" alt=""/>
                </h2>

                <div class="news-content">
<?php

$term_id = 0;
$base_url = home_url('/news/');

if( ! empty($_GET['t']) && is_numeric($_GET['t']) ) {
  $term_id = $_GET['t'];
}

$args = array(
    'hide_empty' => false,
    'orderby' => 'term_order',
    'exclude' => 1,
);

$news_cats = get_terms( 'category', $args );
?>
                    <ul class="news-category-tab">
                        <li <?php if( $term_id == 0 ): ?>class="active"<?php endif; ?>><a href="<?php echo $base_url; ?>">最新</a></li>
<?php
if( ! is_wp_error($news_cats) ):
  foreach( $news_cats as $news_cat ):
?>
                        <li <?php if( $term_id == $news_cat->term_id ): ?>class="active"<?php endif; ?>><a href="<?php echo add_query_arg( 't', $news_cat->term_id, $base_url ); ?>"><?php echo $news_cat->name; ?></a></li>
<?php
  endforeach;
endif;
?>
                    </ul>
<?php

$news_list_paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;

$args = array(
    'post_type'      => 'post',
    'post_status' => 'publish',
    'paged'	         => $news_list_paged,
);

if( $term_id > 0 ) {
  $args['cat'] = $term_id;
}


$post_query = new WP_Query( $args );

if ( $post_query->have_posts() ) :
?>
                    <div class="news-list">
                        <ul>
<?php
  while ( $post_query->have_posts() ) :
    $post_query->the_post();
?>
                            <li class="news-list-item">
                                <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                                    <div class="news-list-icon">
                                        <?php the_post_thumbnail('news-avatar'); ?>
                                    </div>
                                    <div class="news-list-title">
                                        <span><?php the_time('Y/m/d'); ?></span>
                                        <strong><?php the_title(); ?></strong>
                                    </div>
                                </a>
                            </li>
<?php
  endwhile;
?>
                        </ul>
                    </div>

<?php
else:
?>
                    <div class="news-list" style="color:#fff;">找不到最新消息</div>
<?php
endif;


if ($post_query->max_num_pages > 1):
  numeric_posts_nav($post_query);
endif;
?>
                </div>
            </div>
        </div>


<?php
wp_reset_postdata();

get_footer();
