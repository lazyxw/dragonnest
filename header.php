<?php
/**
 * The header for our theme
 */

// $nav_menu = wp_get_nav_menu_items('主選單');
$nav_menu = array(
  (object) array('title' => '最新消息', 'url' => '/news/'),
  (object) array('title' => '影音圖集', 'url' => '/media/'),
  (object) array('title' => '兌換虛寶', 'url' => 'https://dn-code.funplusgame.com/'),
  (object) array('title' => '客服支援', 'url' => 'https://funplus.helpshift.com/a/龍之谷/'),
  (object) array('title' => '儲值方式', 'url' => 'http://dn-shop.funplusgame.com'),
  (object) array('title' => '巴哈論壇', 'url' => GAMER_GROUP_URL),
  (object) array('title' => 'Facebook', 'url' => FB_PAGE_URL),
);

$body_class = 'inner-bg';
if( is_front_page() ) {
  $body_class = 'landing-bg';
}

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<?php wp_head(); ?>
<?php require_once get_template_directory() . '/inc/ga.php'; ?>
</head>

<body <?php body_class($body_class); ?>>
<!-- <body class="<?php echo $body_class; ?>"> -->
<?php if( ! is_front_page() ): ?>
<div id="page" class="site">
<?php endif; ?>
  <div class="container">
    <nav class="main-nav main-nav-lg">
      <a href="<?php echo home_url(); ?>" class="brand-logo">龍之谷</a>
<?php if( ! empty( $nav_menu ) ): ?>
      <ul class="main-nav-list">
<?php

  $menu_fb = null;

  foreach( $nav_menu as $menu ):
    if( $menu->title == 'Facebook' ) {
      $menu_fb = $menu;
      continue;
    }

?>
        <li><a href="<?php echo $menu->url; ?>" title="<?php echo $menu->title; ?>"><?php echo $menu->title; ?></a></li>
  <?php endforeach; ?>
      </ul>
  <?php if( ! empty($menu_fb) ): ?>
      <a href="<?php echo $menu_fb->url; ?>" class="nav-facebook">
        <i class="fa fa-facebook-official" aria-hidden="true"></i>
      </a>
  <?php endif; ?>
<?php endif; ?>
    </nav>

    <div class="nav-bar-mobile visible-xs-inline-block">
      <div class="row">
        <div class="col-xs-4">
            <a href="<?php echo home_url(); ?>" class="brand-logo">龍之谷</a>
        </div>

        <div class="col-xs-5 text-center">
            <a href="<?php echo get_option(SITE_DOWNLOAD_OFFICIAL); ?>" title="" class="btn btn-block btn-sm btn-primary mobile-download-btn">
                <img src="<?php echo TEMPLATE_DIR_URI; ?>/images/download-btn-mobile.png" alt="" />
            </a>
        </div>

        <div class="col-xs-3">
            <button class="hamburger-btn">
              <img src="<?php echo TEMPLATE_DIR_URI; ?>/images/hamburger.svg" alt=""/>
            </button>
        </div>

      </div>


      <div class="mobile-main-menu show-nav">
        <span class="btn-close">
          <i class="fa fa-times" aria-hidden="true"></i>
        </span>
<?php if( ! empty( $nav_menu ) ): ?>
        <ul class="mobile-main-menu-list">
<?php
  foreach( $nav_menu as $menu ):
    if( $menu->title == 'Facebook' ) {
      $menu_fb = $menu;
      continue;
    }

?>
          <li><a href="<?php echo $menu->url; ?>" title="<?php echo $menu->title; ?>"><?php echo $menu->title; ?></a></li>
  <?php endforeach; ?>
  <?php if( ! empty($menu_fb) ): ?>
          <li><a class="facebook-btn" href="<?php echo $menu_fb->url; ?>" title="<?php echo $menu_fb->title; ?>"><i class="fa fa-facebook-official" aria-hidden="true"></i> <?php echo $menu_fb->title; ?></a></li>
  <?php endif; ?>
        </ul>
<?php endif; ?>

      </div>
    </div>
