<?php
/*
 * Template name: pre register
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<?php wp_head(); ?>
<?php require_once get_template_directory() . '/inc/ga.php'; ?>
</head>
<style>
@media (max-width: 767px) {
  .pre-register-logo {
    right:initial;
    top: 1px;
    left: initial;
  }
}
</style>
<body class="register-bg">

<?php if( is_user_logged_in() ): ?>
    <a href="<?php echo wp_logout_url( home_url('/pre-reg/') ); ?>" border="0" style="position: absolute;top:20px;right:20px;"><img src="/wp-content/uploads/2017/09/btn-logout.png"></a>
<?php endif; ?>

    <a href="<?php echo home_url(); ?>" class="pre-register-logo">
        <img src="<?php echo TEMPLATE_DIR_URI; ?>/images/main-logo.png" alt="龍之谷" />
    </a>

    <div class="pre-register-heading">
        <h1>
            <img src="<?php echo TEMPLATE_DIR_URI; ?>/images/pre-register-heading.png" alt="" style="visibility:hidden;" />
        </h1>
    </div>
<?php
$pre_reg_dueday = strtotime(get_option(SITE_PRE_REG_DUEDAY));
if( ! ( $pre_reg_dueday !== false && time() > $pre_reg_dueday ) ):

$boundary = array(
  0,
  5000,
  10000,
  20000,
  40000,
  60000,
  100000,
  150000
);


$usercount = get_option( SITE_USER_COUNT );

$useroffset = get_option( SITE_USER_OFFSET );
if( is_numeric($useroffset) ) {
    $usercount += $useroffset;
}

// if( isset($_GET['c']) && is_numeric($_GET['c']) ){
//   $usercount = $_GET['c'];
// }

$step = 0;
$progress = 0;
$bonus_level = 0;

$each_level = 100 / count($boundary);

foreach ( $boundary as $i => $range ) {
  if( $usercount >= $range && $usercount < $boundary[$i+1] ) {
    $percentage = $each_level * ( $usercount - $range ) / ( $boundary[$i+1] - $range);
    $progress += $percentage;
    $bonus_level = $i;
    break;
  }

  $progress += $each_level;
}

if ( $usercount >= end($boundary) ) {
  $progress = 100;
  $bonus_level = count($boundary) - 1;
}

$bar_type = 'and-' . ($boundary[$bonus_level] / 1000) . 'k';
$progress = round( $progress, 2 );

?>

    <div class="pre-register-menbers">
        <h2><img src="<?php echo TEMPLATE_DIR_URI; ?>/images/now-members-heading.png" alt="" /></h2>
        <div class="menbers-number">
            <?php echo $usercount; ?>
        </div>

        <p>登錄到達目標人數，即可擊敗當前BOSS，進入下一階段</p>
    </div>


    <div class="container">
        <div class="boss-fighting-mobile">
            <div class="visible-xs boss-fighting-mobile-slider">
                <div class="boss-fighting-mobile-item <?php if( $bonus_level == 0 ) echo 'active'; else if( $bonus_level >= 1 ) echo 'success'; ?>">
                    <div class="boss-title text-center">
                        <span>累積登錄達到</span><b>5000人</b>
                    </div>
                    <div class="success-label"><span>已達標</span></div>
                    <img src="<?php echo TEMPLATE_DIR_URI; ?>/images/boss-avatar-1.png" alt="" />
                </div>

                <div class="boss-fighting-mobile-item <?php if( $bonus_level == 1 ) echo 'active'; else if( $bonus_level >= 2 ) echo 'success'; ?>">
                    <div class="boss-title text-center">
                        <span>累積登錄達到</span><b>10000人</b>
                    </div>
                    <div class="success-label"><span>已達標</span></div>
                    <img src="<?php echo TEMPLATE_DIR_URI; ?>/images/boss-avatar-2.png" alt="" />
                </div>

                <div class="boss-fighting-mobile-item <?php if( $bonus_level == 2 ) echo 'active'; else if( $bonus_level >= 3 ) echo 'success'; ?>">
                    <div class="boss-title text-center">
                        <span>累積登錄達到</span><b>20000人</b>
                    </div>
                    <div class="success-label"><span>已達標</span></div>
                    <img src="<?php echo TEMPLATE_DIR_URI; ?>/images/boss-avatar-3.png" alt="" />
                </div>

                <div class="boss-fighting-mobile-item <?php if( $bonus_level == 3 ) echo 'active'; else if( $bonus_level >= 4 ) echo 'success'; ?>">
                    <div class="boss-title text-center">
                        <span>累積登錄達到</span><b>40000人</b>
                    </div>
                    <div class="success-label"><span>已達標</span></div>
                    <img src="<?php echo TEMPLATE_DIR_URI; ?>/images/boss-avatar-new-4.png" alt="" />
                </div>

                <div class="boss-fighting-mobile-item <?php if( $bonus_level == 4 ) echo 'active'; else if( $bonus_level >= 5 ) echo 'success'; ?>">
                    <div class="boss-title text-center">
                        <span>累積登錄達到</span><b>60000人</b>
                    </div>
                    <div class="success-label"><span>已達標</span></div>
                    <img src="<?php echo TEMPLATE_DIR_URI; ?>/images/boss-avatar-new-5.png" alt="" />
                </div>
                <div class="boss-fighting-mobile-item <?php if( $bonus_level == 5 ) echo 'active'; else if( $bonus_level >= 6 ) echo 'success'; ?>">
                    <div class="boss-title text-center">
                        <span>累積登錄達到</span><b>100000人</b>
                    </div>
                    <div class="success-label"><span>已達標</span></div>
                    <img src="<?php echo TEMPLATE_DIR_URI; ?>/images/boss-avatar-new-6.png" alt="" />
                </div>
                <div class="boss-fighting-mobile-item <?php if( $bonus_level == 6 ) echo 'active'; else if( $bonus_level >= 7 ) echo 'success'; ?>">
                    <div class="boss-title text-center">
                        <span>累積登錄達到</span><b>150000人</b>
                    </div>
                    <div class="success-label"><span>已達標</span></div>
                    <img src="<?php echo TEMPLATE_DIR_URI; ?>/images/boss-avatar-new-7.png" alt="" />
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="boss-fighting">
            <div class="boss-block-top clearfix hidden-xs">
                <div class="boss-2 <?php if( $bonus_level == 1 ) echo 'active'; else if( $bonus_level >= 2 ) echo 'success'; ?>"><span>已達標</span></div>
                <div class="boss-4 <?php if( $bonus_level == 3 ) echo 'active'; else if( $bonus_level >= 4 ) echo 'success'; ?>"><span>已達標</span></div>
                <div class="boss-6 <?php if( $bonus_level == 5 ) echo 'active'; else if( $bonus_level >= 6 ) echo 'success'; ?>"><span>已達標</span></div>
            </div>

            <div class="progress-bar-block <?php echo $bar_type; ?> clearfix">
                <div class="progress-bar-bg">
                    <span class="progress-rate" style="width:<?php echo $progress; ?>%"></span>
                </div>
            </div>

            <div class="boss-block-bottom clearfix hidden-xs">
                <div class="boss-1 <?php if( $bonus_level == 0 ) echo 'active'; else if( $bonus_level >= 1 ) echo 'success'; ?>"><span>已達標</span></div>
                <div class="boss-3 <?php if( $bonus_level == 2 ) echo 'active'; else if( $bonus_level >= 3 ) echo 'success'; ?>"><span>已達標</span></div>
                <div class="boss-5 <?php if( $bonus_level == 4 ) echo 'active'; else if( $bonus_level >= 5 ) echo 'success'; ?>"><span>已達標</span></div>
                <div class="boss-7 <?php if( $bonus_level == 6 ) echo 'active'; else if( $bonus_level >= 7 ) echo 'success'; ?>"><span>已達標</span></div>
                &nbsp;
            </div>
        </div>
    </div>

<?php if(  ! is_user_logged_in() ): ?>
    <div class="pre-register-sns-login">
        <h2 class="text-center"><img src="<?php echo TEMPLATE_DIR_URI; ?>/images/pre-login-heading.png" alt="" /></h2>

        <div class="container">
            <p>搶先登錄討伐BOSS拿獎勵</p>
            <div class="row login-btn">
                <div class="col-sm-6 text-center">
                    <a href="#" onclick="socialLogin('loginFacebook');return false;">
                        <img src="<?php echo TEMPLATE_DIR_URI; ?>/images/facebook-login-btn.png" alt="" />
                    </a>
                </div>

                <div class="col-sm-6 text-center">
                    <a href="#" onclick="socialLogin('loginGoogle');return false;">
                        <img src="<?php echo TEMPLATE_DIR_URI; ?>/images/google-login-btn.png" alt="" />
                    </a>
                </div>
            </div>
        </div>
    </div>

<?php
else:

  $step = 1;

  $user_data = wp_get_current_user();
  $dn_user = get_dn_user( $user_data->ID );

  $phone = get_user_meta($user_data->ID, USER_META_PHONE_NUMBER, true);

  if( empty($phone) ) {
    $phone_value = '';
  } else {
    $phone_value = 'value="' . $phone . '" readonly';
    $step++;
  }
?>

    <div class="pre-register-sns-login">
        <h2 class="text-center"><img src="<?php echo TEMPLATE_DIR_URI; ?>/images/pre-login-heading.png"></h2>

        <div class="container">
            <div class="row">
              <div class="col-sm-12 text-center">

  <?php if( $bonus_level > 0 ): ?>
                <img src="<?php echo TEMPLATE_DIR_URI; ?>/images/got-praise.png" alt="" />
                <br>
                <img src="<?php echo TEMPLATE_DIR_URI; ?>/images/boss-praise-<?php echo $bonus_level; ?>.png" />
  <?php else: ?>
                <p>已登錄</p>
  <?php endif; ?>
              </div>
            </div>

            <div id="section-phone" class="guild-form">
                <div class="row">
                <?php if( empty($phone) ) : ?>
                    <div class="col-sm-6">
                        <h3><img src="<?php echo TEMPLATE_DIR_URI; ?>/images/combine-phone-heading.png" alt=""></h3>
                        <div class="form-group">
                            <div class="form-group space-top-30">
                                <label class="sr-only" for="">請手機號碼</label>
                                <input type="text" class="form-control" id="phone" placeholder="請手機號碼" maxlength="10" <?php echo $phone_value; ?>>
                                <a id="submit-phone-btn" class="space-top-20 space-bottom-20 btn btn-block btn-lg btn-default submit-btn">送 出</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 text-center">
                        <img class="praise-img" src="<?php echo TEMPLATE_DIR_URI; ?>/images/phone-praise.png" alt="" />
                    </div>
                <?php else: ?>
                    <div class="col-sm-12 text-center">
                      <h3 class="text-left"><img src="<?php echo TEMPLATE_DIR_URI; ?>/images/combine-phone-heading.png" alt=""></h3>

                      <br>

                      <img class="praise-img" src="<?php echo TEMPLATE_DIR_URI; ?>/images/phone-praise-done.png" alt="" />
                  </div>
                <?php endif; ?>
                </div>
            </div>
<?php

$guild_id = get_user_meta($user_data->ID, USER_META_GUILD_ID, true);

if( empty( $guild_id ) ) {
  $args = array(
      'post_type'      => 'guild',
      'meta_key' => GUILD_META_COUNT,
      'meta_query' => array(
        array(
            'key'     => GUILD_META_COUNT,
            'value'   => GUILD_MEMBER_LIMIT,
            'compare' => '<',
        ),
      ),
      'orderby'       => array( 'meta_value_num' => 'DESC', 'ID' => 'ASC'),
      'post_status'    => 'publish',
      'posts_per_page' => 5,
  );

  $member_list_html = '';
  $list_title = '熱門公會';
  $posts = get_posts( $args );
  foreach( $posts as $guild ){
    $mcount = get_post_meta($guild->ID, GUILD_META_COUNT, true);
    $member_list_html .= "<li>[" . $guild->ID . '] ' . $guild->post_title . " ( " . $mcount . "人 )" . "</li>\n";
  }

} else {

  if( $step == 2 ) $step++;

  $list_title = '會員列表';
  $member_list_html = '';

  $guild = get_post($guild_id);
  $guild_name = '[' . $guild->ID . ']' . $guild->post_title;
  $nickname = $user_data->display_name;
}


?>

            <div id="section-guild" class="guild-form">
                <div class="row">
                    <div class="col-sm-6">
                        <h3><img src="<?php echo TEMPLATE_DIR_URI; ?>/images/join-guild-heading.png" alt=""></h3>
                        <?php if( empty( $guild_id ) ) : ?>
                        <div class="form-group">
                            <div class="form-group space-top-30">
                                <label class="sr-only" for="">請輸入公會名稱</label>
                                <input id="guild" type="text" class="form-control" id="guild" placeholder="請輸入公會名稱" maxlength="100">
                            </div>
                        </div>

                        <div class="form-group space-top-20">
                            <div class="form-group">
                                <label class="sr-only" for="">輸入暱稱</label>
                                <input type="text" class="form-control" id="nikename" placeholder="輸入暱稱" maxlength="100">
                            </div>
                        </div>

                        <a id="submit-guild-btn" class="space-top-20 space-bottom-20 btn btn-block btn-lg btn-default submit-btn">創建公會</a>

                        <?php else :?>

                          <div class="form-group" style="font-size:30px;color:#ffffff;line-height:2.5em;padding:20px;">
                            Hi <?php echo $nickname; ?>
                            <br>
                            你已經加入
                            <br>
                            <?php echo $guild_name; ?>
                          </div>

                        <?php endif; ?>
                    </div>

                    <div class="col-sm-6">
                        <div class="form-group">
                          <?php if( empty( $guild_id ) ) : ?>
                            <select id="guild" class="selectpicker show-tick form-control" data-live-search="true" title='請選擇公會：'>
                            </select>
                          <?php else: ?>
                            <div style="height:53px;">&nbsp;</div>
                          <?php endif; ?>
                        </div>
                        <div class="guild-member-list">
                            <strong id="guild-member-list-title"><?php echo $list_title; ?></strong>

                            <div class="list-block">
                                <ul id="guild-member-list">
                                  <?php echo $member_list_html; ?>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <?php if( empty( $guild_id ) ) : ?>
                    <div class="col-sm-12 text-center">
                        <img class="hidden-xs praise-img" src="<?php echo TEMPLATE_DIR_URI; ?>/images/guild-praise.png" alt="" />
                        <img class="visible-xs praise-img" src="<?php echo TEMPLATE_DIR_URI; ?>/images/guild-praise-mobile.png" alt="" />
                    </div>
                    <?php else: ?>
                    <div class="col-sm-12 text-center">
                        <img class="hidden-xs praise-img" src="<?php echo TEMPLATE_DIR_URI; ?>/images/guild-praise-done.png" alt="" />
                        <img class="visible-xs praise-img" src="<?php echo TEMPLATE_DIR_URI; ?>/images/guild-praise-mobile-done.png" alt="" />
                    </div>
                 <?php endif; ?>
                </div>
            </div>

        </div>
    </div>

<?php

$vote = get_user_meta($user_data->ID, USER_META_VOTE_VIDEO, true);

// $video1 = get_option(SITE_VIDEO_1);
// $video2 = get_option(SITE_VIDEO_2);
// $video1_title = get_option(SITE_VIDEO_1_TITLE);
// $video2_title = get_option(SITE_VIDEO_2_TITLE);
$video1 = STATIC_SITE_VIDEO_1;
$video2 = STATIC_SITE_VIDEO_2;
$video1_title = STATIC_SITE_VIDEO_1_TITLE;
$video2_title = STATIC_SITE_VIDEO_2_TITLE;
$video1_thumb = getYoutubeThumb($video1);
$video2_thumb = getYoutubeThumb($video2);

?>
    <div style="font-size:30px;color:#ffffff;line-height:2.5em;text-align:center;">
    <div>最新活動與虛寶贈送，都在《龍之谷》粉絲頁</div>
    <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FDragonNestMobile.TW%2F&tabs=timeline&width=180&height=70&small_header=true&adapt_container_width=true&hide_cover=false&show_facepile=false&appId=1854546348137369" width="180" height="72" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" align="middle"></iframe> </div>

    <div id="section-vote" class="video-voteing">
        <img src="<?php echo TEMPLATE_DIR_URI; ?>/images/pre-item-bg-left.png" class="float-bg-1" alt="" />
        <img src="<?php echo TEMPLATE_DIR_URI; ?>/images/pre-item-bg.png" class="float-bg-2" alt="" />

        <div class="container">
            <h2 class="text-center"><img src="<?php echo TEMPLATE_DIR_URI; ?>/images/voteing-heading.png" alt="" /></h2>

<?php if( empty( $vote ) ) : ?>
            <p>
                《龍之谷》手游公測即將盛大來襲，多項福利將一同助陣，使回歸基谷的勇士們可以輕鬆跟上冒險大部隊的步伐。
            </p>

            <div class="row space-top-80">
                <div class="col-sm-12 text-center">
                    <img class="hidden-xs praise-img" src="<?php echo TEMPLATE_DIR_URI; ?>/images/voteing-praise.png" alt="" />
                    <img class="visible-xs praise-img" src="<?php echo TEMPLATE_DIR_URI; ?>/images/voteing-praise-mobile.png" alt="" />
                </div>
            </div>

            <div class="row space-top-80">
                <div class="col-sm-6 text-right">
                    <div class="media-list-item">
                        <a class="video-cover mfp-iframe" href="<?php echo $video1; ?>" style="background-image:url('<?php echo $video1_thumb; ?>')">
                            <i class="fa fa-play-circle" aria-hidden="true"></i>
                        </a>
                        <div class="video-title"><?php echo $video1_title; ?></div>
                    </div>
                    <div class="text-center">
                        <a class="btn btn-lg btn-primary voteing-btn vote-btn" data-i="1">投票</a>
                    </div>
                </div>

                <div class="col-sm-6 text-left">
                    <div class="media-list-item">
                        <a class="video-cover mfp-iframe" href="<?php echo $video2; ?>" title="" style="background-image:url('<?php echo $video2_thumb; ?>')">
                            <i class="fa fa-play-circle" aria-hidden="true"></i>
                        </a>
                        <div class="video-title"><?php echo $video2_title; ?></div>
                    </div>
                    <div class="text-center">
                        <a class="btn btn-lg btn-primary voteing-btn vote-btn" data-i="2">投票</a>
                    </div>
                </div>
            </div>
<?php else: if( $step == 3 ) $step++; ?>

            <div class="row space-top-80">
                <div class="col-sm-12 text-center">
                    <img class="hidden-xs praise-img" src="<?php echo TEMPLATE_DIR_URI; ?>/images/voteing-praise-done.png" alt="" />
                    <img class="visible-xs praise-img" src="<?php echo TEMPLATE_DIR_URI; ?>/images/voteing-praise-mobile-done.png" alt="" />
                </div>
            </div>
<?php endif; ?>
        </div>
    </div>

    <?php if( isset($_GET['md']) ): ?>
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body" style="text-align:center;">
                <?php if($_GET['md'] == 2): ?>
                    <img class="praise-img" src="<?php echo TEMPLATE_DIR_URI; ?>/images/phone-praise-done.png" alt="" />
                <?php elseif($_GET['md'] == 3): ?>
                    <img class="praise-img" src="<?php echo TEMPLATE_DIR_URI; ?>/images/guild-praise-mobile-done.png" alt="" />
                <?php elseif($_GET['md'] == 4): ?>
                    <img class="praise-img" src="<?php echo TEMPLATE_DIR_URI; ?>/images/voteing-praise-mobile-done.png" alt="" />
                <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
    <?php endif; ?>

<?php endif; ?>

<?php else: ?>
    <div class="pre-register-sns-login">
        <h2 class="text-center" style="color:#ffffff;">活動已結束</h2>
    </div>
<?php endif; ?>

<script type="text/javascript">

function socialLogin( label ) {
    var link = '<?php echo home_url(); ?>/wp-login.php?' + label + '=1&redirect=' + window.location.href;
    if (typeof fbq === 'function' ) {
        fbq('track', 'CompleteRegistration', {content_name: label});
    }

    if (typeof ga === 'function') {
        ga('send', 'event', {
            'eventCategory': 'pre-reg',
            'eventAction': '註冊開始',
            'eventLabel': label,
            'hitCallback': function () {
                window.location = link;
            }
        });
    } else {
        window.location = link;
    }
}

jQuery(document).ready(function($){
    var link = window.location.href.split('?')[0];
    var processing = false;
    var processing_icon = ' <i class="fa fa-spinner fa-spin" aria-hidden="true"></i>';

    $('.boss-fighting-mobile-slider').slick({
        dots: true,
        infinite: false,
        initialSlide: <?php echo $bonus_level; ?>
    });


  function setMemberContent( id ) {
    $('#guild-member-list').html('<i class="fa fa-spinner fa-spin fa-5x" aria-hidden="true"></i>');

    $.ajax({
      type: 'POST',
      dataType: 'json',
      url: '/wp-admin/admin-ajax.php',
      data: 'action=get_guild_members&g=' +id,
      success: function(data) {
        var content = '';
        if (data.length > 0) {

          for(var i in data){
            content += '<li>' + data[i] + '</li>';
          }

          $('#guild-member-list').html(content);

        } else {
          $('#guild-member-list').html('<li>目前沒有成員</li>');
        }
      }
    });
  }

<?php if( is_user_logged_in() ): ?>
  $('a.video-cover').magnificPopup({});
  <?php if( isset($_GET['md']) and in_array($_GET['md'], array(2, 3, 4)) ): ?>
  $('#myModal').modal('show');
  <?php endif; ?>
<?php
  $target_array = array (
    1 => 'section-phone',
    2 => 'section-guild',
    3 => 'section-vote',
  );

  if( isset($target_array[$step]) ):
    $target_id = $target_array[$step];
?>
    $("body").animate({ scrollTop: $('#<?php echo $target_id; ?>').offset().top }, 100);
<?php endif; ?>

  <?php if( empty($phone) ): ?>
    var re = /^[09]{2}[0-9]{8}$/;
    $('#submit-phone-btn').on('click', function(e){
        e.preventDefault();
        var $this = $(this);
        var btn_text = $this.html();

        if( ! processing ) {


            var phone = $('#phone').val();

            if( re.test(phone) ) {
                $this.append(processing_icon);
                processing = true;
                $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    url: '/wp-admin/admin-ajax.php',
                    data: 'action=save_phone&phone=' + phone,
                    success: function(data) {
                        if(data == 1){
                            if (typeof fbq === 'function' ) {
                                fbq('track', '手機號', {content_name: phone});
                            }
                            ga('send', 'event', {
                                'eventCategory': 'pre-reg',
                                'eventAction': '手機號',
                                'eventLabel': phone,
                                'hitCallback': function () {
                                        location.replace(link + "?md=2");
                                    }
                            });
                        } else if(data == -2) {
                            $this.html(btn_text);
                            processing = false;
                            alert('此手機號碼已經有人使用');
                        }
                    }
                });

            } else {
                alert('手機號碼格式錯誤');
            }
        }
    });
  <?php endif; ?>

  <?php if( empty( $guild_id ) ) : ?>
    // bootstrap select
    var bs_select = $('.selectpicker');
    var cur_guild = '';

    bs_select.selectpicker({});

    $.ajax({
      type: 'POST',
      dataType: 'json',
      url: '/wp-admin/admin-ajax.php',
      data: 'action=get_guild_list',
      success: function(data) {

        for(var i in data){
          bs_select.append('<option value="' + data[i].id + '">' + data[i].title + '</option>');
        }

        bs_select.selectpicker('refresh');

      }
    });

    bs_select.on('change', function(e){
      e.preventDefault();
      $('#guild-member-list-title').html('會員列表');
      var selected = $('.selectpicker option:selected').val();
      var selected_name = $('.selectpicker option:selected').html();

      if( selected_name.indexOf(' ( 額滿 )') < 0 ) {
        var guild_name = selected_name.replace(/^\[\d+\]/, "").replace(/ \( \d+人 \)$/, "");
        $( 'input[id="guild"]' ).val(guild_name);
        cur_guild = selected_name;
        $('#submit-guild-btn').html('加入公會');
      }

      setMemberContent(selected);
    });


    $( 'input[id="guild"]' ).autocomplete({
      source: function(name, response) {
        $.ajax({
          type: 'POST',
          dataType: 'json',
          url: '/wp-admin/admin-ajax.php',
          data: 'action=get_guild_list&s='+name.term,
          success: function(data) {
            response(data);
          }
        });
      },
      focus: function( event, ui ) {
        if( $( this ).val() != cur_guild ) {
          $('#submit-guild-btn').html('創建公會');
        }
        return false;
      },
      select: function( event, ui ) {
        $( this ).val( ui.item.title );
        cur_guild = ui.item.title;
        $('#submit-guild-btn').html('加入公會');
        return false;
      },
      change: function( event, ui ) {
        if( $( this ).val() != cur_guild ) {
          $('#submit-guild-btn').html('創建公會');
        }
        return false;
      },
      minLength: 2
    })
    .data("ui-autocomplete")._renderItem = function (ul, item) {
        var newText = String(item.title).replace(
                new RegExp(this.term, "gi"),
                "<span class='ui-highlight'>$&</span>");

        return $("<li></li>")
            .data("item.autocomplete", item.title)
            .append("<a>" + newText + "</a>")
            .appendTo(ul);
    };

    $('#submit-guild-btn').on('click', function(e){
        var $this = $(this);
        var btn_text = $this.html();
        e.preventDefault();

        if( ! processing ) {
            var guild = $('input[id="guild"]').val();
            if(guild == ''){
                alert('請輸入公會名稱');
                return;
            }

            var nickname = $('#nikename').val();
            if(nickname == ''){
                alert('請輸入暱稱');
                return;
            }

            $(this).append(processing_icon);

            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: '/wp-admin/admin-ajax.php',
                data: 'action=save_guild&guild=' + guild + '&nickname=' + nickname,
                success: function(data) {
                    if(data == 1){
                        if (typeof fbq === 'function' ) {
                            fbq('track', '公會', {content_name: guild});
                        }
                        ga('send', 'event', {
                            'eventCategory': 'pre-reg',
                            'eventAction': '公會',
                            'eventLabel': guild,
                            'hitCallback': function () {
                                    location.replace(link + "?md=3");
                                }
                        });
                    } else if(data == -1) {
                        $this.html(btn_text);
                        processing = false;
                        alert('公會人數已滿');
                    } else if(data == -2) {
                        $this.html(btn_text);
                        processing = false;
                        alert('此暱稱已經有人使用');
                    }
                }
            });
        }

    });
  <?php else: ?>
    setMemberContent(<?php echo $guild_id; ?>);
  <?php endif; ?>

  <?php if( empty( $vote ) ) : ?>
    $('.vote-btn').on('click', function(e){
      e.preventDefault();
      $(this).append(processing_icon);
      voteVideo($(this).data('i'));
    });

    function voteVideo(i) {
        if( ! processing ) {
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: '/wp-admin/admin-ajax.php',
                data: 'action=vote_video&v=' + i,
                success: function(data) {
                    if(data == 1){
                        if (typeof fbq === 'function' ) {
                            fbq('track', '投票', {content_name: i});
                        }
                        ga('send', 'event', {
                            'eventCategory': 'pre-reg',
                            'eventAction': '投票',
                            'eventLabel': i,
                            'hitCallback': function () {
                                    location.replace(link + "?md=4");
                                }
                        });
                    }
                }
            });
        }
    }
  <?php endif; ?>
<?php endif; ?>
});
</script>

    <footer class="container ">
        <div class="row ">
            <div class="col-md-6 col-sm-6 col-xs-12  text-right">
                    <img src="<?php echo TEMPLATE_DIR_URI; ?>/images/12age-banner.png" alt="" />
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <span>本遊戲內容涉及暴力與清涼服飾。<br>
                        部分內容另外收費。注意使用時間.避免沉迷遊戲。 <br>
                        用戶協議：龍之谷使用者協議 個人咨詢及隱私保護政
                </span>
            </div>
        </div>
    </footer>

<?php wp_footer(); ?>
</body>

</html>