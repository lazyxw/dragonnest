<?php

get_header(); ?>

        <div class="inner-page-block">
            <div class="inner-page-banner-img">
                <img src="<?php echo TEMPLATE_DIR_URI; ?>/images/header-banner-news.png" alt="" />
            </div>
<?php
while ( have_posts() ) :
	the_post();
?>

            <div class="inner-content">
                <div class="decorative-borders-center"></div>

                <h2 class="heading text-center hidden-xs">
                    <img src="<?php echo TEMPLATE_DIR_URI; ?>/images/heading-lg-news.png" alt=""/>
                </h2>

                <h2 class="heading text-center visible-xs">
                    <img src="<?php echo TEMPLATE_DIR_URI; ?>/images/heading-xs-news.png" alt=""/>
                </h2>

                <div class="news-content">
                    <div class="row news-detail space-top-30">
                        <div class="col-sm-4 text-center">
                              <a href="<?php echo home_url('/news'); ?>" class="btn btn-default space-top-30"><i class="fa fa-chevron-left" aria-hidden="true"></i> 回到列表</a>
                        </div>
                        <div class="col-sm-8">
                            <h3><?php the_title(); ?></h3>

                            <div class="row">
                                <div class="col-sm-12">
                                    <span class="news-date"><i class="fa fa-calendar" aria-hidden="true"></i> <?php the_date(); ?></span>
                                </div>
                            </div>

                            <?php the_content(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<?php
endwhile;


get_footer();
