<?php
/*
 * Template name: code redemption
 */

$pf = isset($_REQUEST['pf']) ? trim($_REQUEST['pf']) : 'dn_haiwai_funplustest';
$group_id = isset($_REQUEST['group_id']) ? trim($_REQUEST['group_id']) : '';
  $role_name = isset($_REQUEST['role_name']) ? trim($_REQUEST['role_name']) : '';
$role_id = isset($_REQUEST['role_id']) ? trim($_REQUEST['role_id']) : '';

get_header();?>
<script src='https://www.google.com/recaptcha/api.js'></script>
        <div class="inner-page-block">
            <div class="inner-page-banner-img">
                <img src="<?php echo TEMPLATE_DIR_URI; ?>/images/header-banner-gifts.png" alt="" />
            </div>


            <div class="inner-content">
                <div class="decorative-borders-center"></div>

                <div class="redeem-content text-center">
                    <div class="border-heading">
                        <h2>兌換虛寶</h2>
                    </div>

                    <?php if( wp_is_mobile() ): ?>
                    <p class="text-center">
                        歡迎來到龍之谷的兌換虛寶頁面！您可以在這裡兌換從各種活動獲得的兌換碼。
                    </p>

                    <form>
                    <div class="gold-well">
                            <input type="hidden" value="<?php echo $pf; ?>" name="pf" id="game-pf"/>
                            <input type="hidden" value="<?php echo $role_id; ?>" name="role_id" id="game-role-id"/>
                            <div class="form-group">
                                <select class="form-control" id="game-group-id">
                                <?php if($group_id) : ?>
                                    <option selected="selected" value="<?php echo $group_id; ?>"><?php echo $group_id; ?>服</option>
                                <?php else: ?>
                                    <option>選擇伺服器</option>
                                <?php endif; ?>
                                </select>

                            </div>


                            <div class="form-group">
                                <select class="form-control" id="game-role-name">
                                <?php if($role_name) : ?>
                                    <option selected="selected" value="<?php echo $role_name; ?>"><?php echo $role_name; ?></option>
                                <?php else: ?>
                                    <option>選擇角色</option>
                                <?php endif; ?>
                                </select>

                            </div>


                            <div class="form-group">
                                <div class="form-group">
                                    <label class="sr-only" for="">輸入兌換碼</label>
                                    <input type="text" class="form-control" id="cdkey" placeholder="輸入兌換碼">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="g-recaptcha" data-sitekey="6LcmaScUAAAAAEnOhbK8A5VXAc4uHY4y7BhhBKzP" data-size="compact" style="margin:0 auto;display:table;"></div>
                            </div>
                    </div>


                    <div class=" row space-top-40 space-bottom-30 ">
                        <div class="col-sm-12 text-center ">
                            <a title=" " class="btn btn-lg btn-default submit-btn " id="confirm-exchange">
                                    兌 換
                                </a>
                        </div>
                    </div>
                    </form>
                    <?php else: ?>
                    <p class="text-center">
                    兌換虛寶僅提供手機端使用
                    </p>
                    <?php endif; ?>
                </div>

            </div>
        </div>

<script>
jQuery(document).ready(function($){
  $('#confirm-exchange').click(function() {
      var pf = $('#game-pf').val();
      var group_id = $('#game-group-id').val();
      var role_name = $('#game-role-name').val();
      var role_id = $('#game-role-id').val();
      var cdkey = $('#cdkey').val();

      if((pf=="") || (group_id=="") || (role_name=="") || (role_id=="")) {
          alert('缺少玩家必要信息');
          return false;
      }

      if(cdkey=="") {
          alert('請輸入禮包碼');
          return false;
      }

      //跨域（可跨所有域名）
      var url = 'http://gameadm.dn.123u.com/api/dn_exchange_cdkey.php';
      var fields = '?pf='+pf+'&group_id='+group_id+'&role_name='+role_name+'&role_id='+role_id+'&cdkey='+cdkey;
      //var fields = "{id: 0, action: '1'}"
      $.getJSON(url+fields+"&jsoncallback=?", function(json){
          alert(json.msg);
          return false;
      });
  });
});
</script>

<?php
get_footer();
