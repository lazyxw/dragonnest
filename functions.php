<?php
/**
 * Dragon Nest TW functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Dragon_Nest_TW
 */

require get_template_directory() . '/inc/const.php'; // Load constant variable.
require get_template_directory() . '/inc/helper.php'; // load herlper.
require get_template_directory() . '/inc/ajax.php'; // Load Ajax function.
require get_template_directory() . '/inc/custom_post_type.php'; // Load custom post type.
require get_template_directory() . '/inc/theme_settings.php'; // load theme settings
require get_template_directory() . '/inc/custom_user_table.php'; // dedicated user table to export
require get_template_directory() . '/inc/class_dn_user_table.php'; // dedicated user table to export
require get_template_directory() . '/inc/tgm_config.php';
require get_template_directory() . '/inc/class-tgm-plugin-activation.php';

require get_template_directory() . '/cli/command.php';


if ( ! function_exists( 'dragon_nest_tw_setup' ) ) :
function dragon_nest_tw_setup() {
	load_theme_textdomain( 'dragon-nest-tw', get_template_directory() . '/languages' );

	add_theme_support( 'title-tag' );
	add_theme_support( 'post-thumbnails' );
	add_theme_support( 'menus' );

	add_image_size( 'news-avatar', 60, 60 );
	add_image_size( 'home-news-avatar', 35, 35 );
	add_image_size( 'video-cover', 295, 185 );

	add_theme_support( 'html5', array(
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// register_nav_menu('dragon-nest-main-nav', "主選單");
}
endif;
add_action( 'after_setup_theme', 'dragon_nest_tw_setup' );

/**
 * Enqueue scripts and styles.
 */
function dragon_nest_tw_scripts() {

	// load bootstrap css
	wp_enqueue_style( 'bootstrap-css', 'https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css' );
	wp_enqueue_style( 'font-awesome', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css', false, '4.7.0' );
	// load bootstrap-select-ui css
	wp_enqueue_style( 'bootstrap-select-css', 'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/css/bootstrap-select.min.css' );
	// load font-awesome css
	// load jquery-ui css
	wp_enqueue_style( 'jquery-ui', 'https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css' );
	// load Magnific Popup core CSS
	wp_enqueue_style( 'magnific-popup', 'https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css' );
	// load Dragon Nest TW styles
	wp_enqueue_style( 'dragon-nest-tw-style', get_template_directory_uri() . '/css/main0912.css' );



	wp_enqueue_script( 'jquery' );
	wp_enqueue_script( 'jquery-ui-core' );
  wp_enqueue_script( 'jquery-ui-autocomplete' );

	// load bootstrap js
	wp_enqueue_script('bootstrapjs', 'https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js', array() );
	wp_enqueue_script('bootstrap-select', 'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/js/bootstrap-select.min.js', array('bootstrapjs') );
	wp_enqueue_script('bootstrap-select-lang', 'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/js/i18n/defaults-zh_TW.min.js', array('bootstrapjs', 'bootstrap-select') );
	wp_enqueue_script('magnific-popup', 'https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js', array('jquery') );

	wp_enqueue_script( 'html5shiv', 'https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js' );
	wp_script_add_data( 'html5shiv', 'conditional', 'lt IE 9' );

	wp_enqueue_script( 'respond', 'https://oss.maxcdn.com/respond/1.4.2/respond.min.js' );
	wp_script_add_data( 'respond', 'conditional', 'lt IE 9' );

	// if( is_front_page() ) {
		wp_enqueue_style( 'animate.css', 'https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css' );
		wp_enqueue_style( 'slick', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css' );
		wp_enqueue_style( 'slick-theme', 'https://dn.funplusgame.com/wp-content/themes/dragon_nest_tw/css/slick-theme.css' );
		wp_enqueue_script('slick-carousel', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.js' );

	// }

}
add_action( 'wp_enqueue_scripts', 'dragon_nest_tw_scripts' );


function dragon_nest_tw_password_form() {
    global $post;
    $label = 'pwbox-'.( empty( $post->ID ) ? rand() : $post->ID );
    $o = '<form action="' . esc_url( site_url( 'wp-login.php?action=postpass', 'login_post' ) ) . '" method="post">
    <div class="d-block mb-3">' . __( "To view this protected post, enter the password below:", "dragon-nest-tw" ) . '</div>
    <div class="form-group form-inline"><label for="' . $label . '" class="mr-2">' . __( "Password:", "dragon-nest-tw" ) . ' </label><input name="post_password" id="' . $label . '" type="password" size="20" maxlength="20" class="form-control mr-2" /> <input type="submit" name="Submit" value="' . esc_attr__( "Submit", "dragon-nest-tw" ) . '" class="btn btn-primary"/></div>
    </form>';
    return $o;
}
add_filter( 'the_password_form', 'dragon_nest_tw_password_form' );


/* Disable WordPress Admin Bar for all users but admins. */
show_admin_bar(false);


function disable_dashboard() {
    if (!is_user_logged_in()) {
        return null;
    }

    if ( ! defined('DOING_AJAX') && current_user_can('subscriber') && is_admin()) {
		wp_redirect(home_url());
        exit;
    }
}
add_action('admin_init', 'disable_dashboard');

function auto_redirect_after_logout(){
	wp_redirect( home_url('/pre-reg/') );
	die();
}
add_action('wp_logout','auto_redirect_after_logout');

// function acme_login_redirect( $redirect_to, $request, $user  ) {
// 	return ( is_array( $user->roles ) && in_array( 'subscriber', $user->roles ) ) ? home_url('/pre-reg/') : $redirect_to;
// }
// add_filter( 'login_redirect', 'acme_login_redirect', 10, 3 );