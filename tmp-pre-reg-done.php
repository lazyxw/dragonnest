<?php
/*
 * Template name: pre register done
 */

$target_url = '/pre-reg/';

global $post;
$event_label = $post->post_name;

if(  ! is_user_logged_in() ) {
    wp_redirect($target_url);
    die();
}

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<?php wp_head(); ?>
<?php require_once get_template_directory() . '/inc/ga.php'; ?>
</head>

<body>

    <div class="text-center">
        <h2>登錄中，請稍候  <i class="fa fa-spinner fa-spin" aria-hidden="true"></i></h2>
        <div><a href="<?php echo $target_url; ?>" style="color:blue;text-decoration:underline;">如果你的瀏覽器沒有自動跳轉，請點擊此鏈接</a></div>
    </div>

<script type="text/javascript">
jQuery(document).ready(function($){
    if (typeof fbq === 'function' ) {
        fbq('track', 'CompleteRegistration', {content_name: event_label});
    }

    if (typeof ga === 'function') {
        ga('send', 'event', {
            'eventCategory': 'pre-reg',
            'eventAction': '註冊成功',
            'eventLabel': '<?php echo $event_label; ?>',
            'hitCallback': function () {
                    setInterval(function(){ location.replace("<?php echo $target_url; ?>"); }, 3000);
                }
        });
    }
});
</script>

<?php wp_footer(); ?>
</body>

</html>